package omdb;

import net.thumbtack.cinematheque.omdb.api.ApiFactory;
import net.thumbtack.cinematheque.omdb.entity.OMDBMovie;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class OmdbGetMovieTest {
    @Test
    public void getMovieTest() throws IOException {
        String title = "the matrix";
        OMDBMovie reachedMovie = ApiFactory.getMovieFromOMDB(title).execute().body();

        Assert.assertNotNull(reachedMovie.getActors());
        Assert.assertNotNull(reachedMovie.getImdbRating());
        Assert.assertNotNull(reachedMovie.getPlot());
        Assert.assertNotNull(reachedMovie.getPoster());
        Assert.assertNotNull(reachedMovie.getTitle());
        Assert.assertNotNull(reachedMovie.getYear());
    }

    @Test
    public void importNonExistsMovie() throws IOException {
        String title = "NotFoundMovieOneTwoThreeFourFive";
        OMDBMovie reachedMovie = ApiFactory.getMovieFromOMDB(title).execute().body();
        Assert.assertNull(reachedMovie.getTitle());
    }
}
