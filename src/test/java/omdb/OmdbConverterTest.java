package omdb;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.exceptions.checkers.MovieChecker;
import net.thumbtack.cinematheque.omdb.api.ApiFactory;
import net.thumbtack.cinematheque.omdb.converters.OmdbMovieConverter;
import net.thumbtack.cinematheque.omdb.entity.OMDBMovie;
import org.junit.Test;

import java.io.IOException;

public class OmdbConverterTest {

    @Test
    public void convertMovieTest() throws IOException {
        String title = "the matrix";
        OMDBMovie reachedMovie = ApiFactory.getMovieFromOMDB(title).execute().body();

        OmdbMovieConverter omdbMovieConverter = new OmdbMovieConverter(reachedMovie);

        Movie movie = new Movie(omdbMovieConverter.getMovieYear(), omdbMovieConverter.getMovieTitle(), omdbMovieConverter.getMovieDescription());
        movie.setStars(omdbMovieConverter.getMovieStars());
        movie.setRank(omdbMovieConverter.getMovieRank());
        movie.setImage(omdbMovieConverter.getMovieImage());

        MovieChecker.checkMovieData(movie);

    }
}
