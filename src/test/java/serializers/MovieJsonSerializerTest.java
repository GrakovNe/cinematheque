package serializers;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.serializers.CustomMovieSerializer;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

public class MovieJsonSerializerTest {
    @Test
    public void movieSerializerTest() throws IOException {
        Movie movie = new Movie(1900, "Movie title", "Movie description");
        movie.setId(1L);

        Set< Star > stars = new HashSet<>();

        Star star = new Star("John", "Smith");
        star.setId(1L);
        stars.add(star);

        star = new Star("Ivan", "Ivanov");
        star.setId(2L);
        stars.add(star);

        star = new Star("Ioann", "Vasilyevich", "Rurikov");
        star.setId(3L);
        stars.add(star);

        movie.setStars(stars);

        Rank rank = new Rank(movie, 4);
        movie.setRank(rank);

        StringWriter stringWriter = new StringWriter();
        JsonGenerator jsonGenerator = new JsonFactory().createGenerator(stringWriter);
        CustomMovieSerializer customMovieSerializer = new CustomMovieSerializer();
        customMovieSerializer.serialize(movie, jsonGenerator, null);
        jsonGenerator.flush();
        String result = stringWriter.toString();

        String accuracyResult = "{\"id\":1,\"title\":\"Movie title\",\"year\":1900,\"rank\":4,\"stars\":[{\"id\":1,\"name\":\"John Smith\"},{\"id\":2,\"name\":\"Ivan Ivanov\"},{\"id\":3,\"name\":\"Ioann Vasilyevich Rurikov\"}]}";
        Assert.assertEquals(accuracyResult, result);

    }
}
