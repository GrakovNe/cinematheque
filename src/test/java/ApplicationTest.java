import net.thumbtack.cinematheque.Application;
import org.junit.Test;

public class ApplicationTest {

    @Test
    public void startupTest(){
        String[] params = new String[1];
        params[0] = "--server.port=9090";
        Application.main(params);
    }
}
