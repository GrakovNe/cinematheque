package rest;

import net.thumbtack.cinematheque.Application;
import net.thumbtack.cinematheque.controllers.rest.MovieControllerRest;
import net.thumbtack.cinematheque.controllers.rest.RankControllerRest;
import net.thumbtack.cinematheque.controllers.rest.StarControllerRest;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import net.thumbtack.cinematheque.repositories.RanksRepository;
import net.thumbtack.cinematheque.repositories.StarsRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ComponentScan({"net.thumbtack.cinematheque"})
public class RankTest {
    @Autowired
    RanksRepository ranksRepository;
    @Autowired
    private MovieControllerRest movieControllerRest;
    @Autowired
    private StarControllerRest starControllerRest;
    @Autowired
    private StarsRepository starsRepository;
    @Autowired
    private MoviesRepository moviesRepository;
    @Autowired
    private RankControllerRest rankControllerRest;
    private Movie movie;

    private OAuth2Authentication user;

    @Before
    public void configureDB() {
        OAuth2Request clientAuthentication = new OAuth2Request(null, null, null, true, null, null, null, null, null);
        Authentication userAuthentication = mock(Authentication.class);
        Map< String, String > userData = new HashMap<>();
        userData.put("id", "1234567890");
        userData.put("name", "JUnitUser");
        when(userAuthentication.getDetails()).thenReturn(userData);

        user = new OAuth2Authentication(clientAuthentication, userAuthentication);
        SecurityContextHolder.getContext().setAuthentication(userAuthentication);

        moviesRepository.deleteAll();
        movie = new Movie(1966, "The good the bad and the ugly", "Interesting plot");
        moviesRepository.save(movie);

        starsRepository.deleteAll();
        Star star = new Star("Clinton", "Clint", "Eastwood");
        starsRepository.save(star);
    }

    @Test
    public void rankMovieTest() {
        Movie movie = new Movie(1937, "Awesome Movie", "Interesting plot");
        movieControllerRest.addMovie(user, movie);

        movieControllerRest.rankMovie(user, movie.getId(), 5);
        Assert.assertEquals(5, movieControllerRest.getMovie(movie.getId()).getRank().getRank());
    }

    @Test
    public void getRankTest() {
        int ranksCounterBefore = rankControllerRest.getRanksList().size();
        Movie movie = new Movie(3210, "Unique movie", "Unique plot");
        movieControllerRest.addMovie(user, movie);
        movieControllerRest.rankMovie(user, movie.getId(), 5);

        Assert.assertEquals(ranksCounterBefore + 1, rankControllerRest.getRanksList().size());
    }

    @Test
    public void getRankByIdentityTest() {
        Movie movie = new Movie(3210, "VeryUnique movie", "VeryUnique plot");
        movieControllerRest.addMovie(user, movie);
        Rank rank = movieControllerRest.rankMovie(user, movie.getId(), 5);

        Rank reachedRank = rankControllerRest.getRankById(rank.getId());

        Assert.assertEquals(rank, reachedRank);
    }

    @Test
    public void updateRankTest() {
        Movie movie = new Movie(1937, "Some Movie", "Interesting plot");
        movieControllerRest.addMovie(user, movie);

        Rank rank = movieControllerRest.rankMovie(user, movie.getId(), 3);
        Assert.assertEquals(3, movieControllerRest.getMovie(movie.getId()).getRank().getRank());

        rank.setRank(4);
        rankControllerRest.updateRank(user, rank.getId(), rank);
        Assert.assertEquals(4, movieControllerRest.getMovie(movie.getId()).getRank().getRank());
    }

}
