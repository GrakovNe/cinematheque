package rest;

import net.thumbtack.cinematheque.Application;
import net.thumbtack.cinematheque.controllers.rest.MovieControllerRest;
import net.thumbtack.cinematheque.controllers.rest.StarControllerRest;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import net.thumbtack.cinematheque.repositories.StarsRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ComponentScan({"net.thumbtack.cinematheque"})
public class StarTest {
    @Autowired
    private MovieControllerRest movieControllerRest;

    @Autowired
    private StarControllerRest starControllerRest;

    @Autowired
    private StarsRepository starsRepository;

    @Autowired
    private MoviesRepository moviesRepository;

    private Movie movie;

    private OAuth2Authentication user;


    @Before
    public void configureDB() {
        OAuth2Request clientAuthentication = new OAuth2Request(null, null, null, true, null, null, null, null, null);
        Authentication userAuthentication = mock(Authentication.class);
        Map< String, String > userData = new HashMap<>();
        userData.put("id", "1234567890");
        userData.put("name", "JUnitUser");
        when(userAuthentication.getDetails()).thenReturn(userData);

        user = new OAuth2Authentication(clientAuthentication, userAuthentication);
        SecurityContextHolder.getContext().setAuthentication(userAuthentication);

        moviesRepository.deleteAll();
        movie = new Movie(1966, "The good the bad and the ugly", "Interesting plot");
        moviesRepository.save(movie);
        starsRepository.deleteAll();
        Star star = new Star("Clinton", "Clint", "Eastwood");
        starsRepository.save(star);

    }

    @Test
    @WithMockUser
    public void addStarTest() {
        int starsCountBefore = starControllerRest.getStarsList().size();
        Star star = new Star("Star", "Starovich", "Starkov");
        starControllerRest.addStar(user, star);

        Assert.assertEquals(starsCountBefore + 1, starControllerRest.getStarsList().size());
    }

    @Test
    public void deleteStarTest() {
        int starsCountBefore = starControllerRest.getStarsList().size();
        Star star = new Star("Non", "Inportant", "Star");
        starControllerRest.addStar(user, star);
        starControllerRest.deleteStar(user, star.getId());

        Assert.assertEquals(starsCountBefore, starControllerRest.getStarsList().size());
    }

    @Test
    public void updateStarTest() {
        Star star = new Star("OldFirstName", "OldMiddleName", "OldLastName");
        starControllerRest.addStar(user, star);
        star.setFirstName("NewFirstName");
        star.setMiddleName("NewMiddleName");
        star.setLastName("NewLastName");

        starControllerRest.updateStar(user, star.getId(), star);

        Star reachedStar = starControllerRest.getStarById(star.getId());

        Assert.assertEquals(star, reachedStar);
    }

    @Test
    public void getStarByNameTest() {
        Star star = new Star("FirstName", "MiddleName", "LastName");
        starControllerRest.addStar(user, star);

        List< Star > reachedStarList = starControllerRest.getStarByFirstName("fiRSTNAme");
        Assert.assertEquals(1, reachedStarList.size());

        reachedStarList = starControllerRest.getStarByLastName("lAsTNaMe");
        Assert.assertEquals(1, reachedStarList.size());
    }

    @Test
    public void attachStarToMovieTest() {
        Star star = new Star("Name", "MiddleName", "LastName");
        starControllerRest.addStar(user, star);

        int moviesStarsCount = movie.getStars().size();
        movieControllerRest.addStar(user, movie.getId(), star.getId());

        Assert.assertEquals(moviesStarsCount + 1, movieControllerRest.getMovie(movie.getId()).getStars().size());
    }

    @Test
    public void detachStarFromMovieTest() {
        Star star = new Star("FirstName", "MiddleName", "LastName");
        starControllerRest.addStar(user, star);

        int moviesStarsCount = movie.getStars().size();
        movieControllerRest.addStar(user, movie.getId(), star.getId());
        movieControllerRest.removeStar(user, movie.getId(), star.getId());

        Assert.assertEquals(moviesStarsCount, movieControllerRest.getMovie(movie.getId()).getStars().size());
    }

    @Test
    public void searchStarTest() {
        int before = starControllerRest.searchStar("NeedToSearch").size();

        Star star = new Star("Famous", "Star", "NeedToSearch");
        starControllerRest.addStar(user, star);

        Assert.assertEquals(before + 1, starControllerRest.searchStar("NeedToSearch").size());
    }

}
