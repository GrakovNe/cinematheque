package rest;

import net.thumbtack.cinematheque.Application;
import net.thumbtack.cinematheque.controllers.rest.MovieControllerRest;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ComponentScan({"net.thumbtack.cinematheque"})
public class MovieTest {

    @Autowired
    private MovieControllerRest movieControllerRest;

    @Autowired
    private MoviesRepository moviesRepository;

    private OAuth2Authentication user;


    @Before
    public void configureDB() {
        OAuth2Request clientAuthentication = new OAuth2Request(null, null, null, true, null, null, null, null, null);
        Authentication userAuthentication = mock(Authentication.class);
        Map< String, String > userData = new HashMap<>();
        userData.put("id", "1234567890");
        userData.put("name", "JUnitUser");
        when(userAuthentication.getDetails()).thenReturn(userData);

        user = new OAuth2Authentication(clientAuthentication, userAuthentication);
        SecurityContextHolder.getContext().setAuthentication(userAuthentication);

        moviesRepository.deleteAll();
        Movie movie = new Movie(1966, "The good the bad and the ugly", "Interesting plot");
        moviesRepository.save(movie);
    }

    @Test
    public void getAllMoviesTest() {
        List< Movie > movieList = movieControllerRest.getMoviesList();
        Assert.assertEquals(1, movieList.size());
    }

    @Test
    public void getMovieByIdTest() {
        List< Movie > movieList = movieControllerRest.getMoviesList();
        Movie movie = movieControllerRest.getMovie(movieList.get(0).getId());

        Assert.assertEquals(movie, movieList.get(0));
    }

    @Test
    public void getRecentMovieTest(){
        movieControllerRest.addMovie(user, new Movie(2000, "A new movie", "Just added movie"));
        List<Movie> recentMovies = movieControllerRest.getRecentMovies();
        Assert.assertEquals("A new movie", recentMovies.get(0).getTitle());
    }

    @Test
    public void getTopMovie(){
        Movie movie = new Movie(1900, "The best movie", "Very best movie");
        movieControllerRest.addMovie(user, movie);
        movieControllerRest.addMovieToWishList(user, movie.getId());
        Movie topMovie = movieControllerRest.getTopMovies().get(0);
        Assert.assertEquals(movie, topMovie);
    }

    @Test
    public void addMovieTest() {
        int moviesCountBefore = movieControllerRest.getMoviesList().size();
        Movie movie = new Movie(2000, "Movie Title", "Interesting plot");
        movieControllerRest.addMovie(user, movie);
        Assert.assertEquals(moviesCountBefore + 1, movieControllerRest.getMoviesList().size());
        Assert.assertEquals("Movie Title", movieControllerRest.getMovie(movie.getId()).getTitle());
        Assert.assertEquals(2000, movieControllerRest.getMovie(movie.getId()).getYear());
    }

    @Test
    public void deleteMovieTest() {
        int moviesCountBefore = movieControllerRest.getMoviesList().size();

        Movie movie = new Movie(2000, "Will be deleted!", "Interesting plot");
        movieControllerRest.addMovie(user, movie);

        movieControllerRest.deleteMovie(user, movie.getId());
        Assert.assertEquals(moviesCountBefore, movieControllerRest.getMoviesList().size());
    }

    @Test
    public void updateMovieTest() {
        Movie movie = new Movie(2000, "Old title", "Interesting plot");
        movieControllerRest.addMovie(user, movie);

        movie.setTitle("New title");
        movieControllerRest.updateMovie(user, movie.getId(), movie);

        Movie reachedMovie = movieControllerRest.getMovie(movie.getId());
        Assert.assertEquals(movie.getTitle(), reachedMovie.getTitle());
    }

    @Test
    public void addToWishListTest() {
        Movie movie = new Movie(2000, "Wished movie", "Interesting plot");
        int moviesCountBefore = movieControllerRest.getWishListMovies(user).size();
        movieControllerRest.addMovie(user, movie);

        movieControllerRest.addMovieToWishList(user, movie.getId());
        Assert.assertEquals(moviesCountBefore + 1, movieControllerRest.getWishListMovies(user).size());
    }

    @Test
    public void addToBookmarkListTest() {
        Movie movie = new Movie(2000, "Bought movie", "Interesting plot");

        int moviesCountBefore = movieControllerRest.getBookmarkListMovies(user).size();
        movieControllerRest.addMovie(user, movie);

        movieControllerRest.addMovieToBookmarkList(user, movie.getId());
        Assert.assertEquals(moviesCountBefore + 1, movieControllerRest.getBookmarkListMovies(user).size());
    }

    @Test
    public void removeFromBookmarkListTest() {
        Movie movie = new Movie(2000, "Not bought movie", "Interesting plot");
        int moviesCountBefore = movieControllerRest.getBookmarkListMovies(user).size();
        movieControllerRest.addMovie(user, movie);

        movieControllerRest.addMovieToBookmarkList(user, movie.getId());
        movieControllerRest.removeMovieFromBookmarkList(user, movie.getId());

        Assert.assertEquals(moviesCountBefore, movieControllerRest.getBookmarkListMovies(user).size());
    }

    @Test
    public void removeFromWishListTest() {
        Movie movie = new Movie(2000, "Not liked movie", "Interesting plot");
        int moviesCountBefore = movieControllerRest.getWishListMovies(user).size();
        movieControllerRest.addMovie(user, movie);

        movieControllerRest.addMovieToWishList(user, movie.getId());
        movieControllerRest.removeMovieFromWishList(user, movie.getId());

        Assert.assertEquals(moviesCountBefore, movieControllerRest.getWishListMovies(user).size());
    }

    @Test
    public void changeWishToBookmarkListTest() {
        Movie movie = new Movie(2000, "Will bye this film... tomorrow", "Interesting plot");
        movieControllerRest.addMovie(user, movie);

        int moviesCountBeforeWish = movieControllerRest.getWishListMovies(user).size();
        int moviesCountBeforeBookmark = movieControllerRest.getBookmarkListMovies(user).size();

        movieControllerRest.addMovieToWishList(user, movie.getId());
        movieControllerRest.addMovieToBookmarkList(user, movie.getId());


        Assert.assertEquals(moviesCountBeforeBookmark + 1, movieControllerRest.getBookmarkListMovies(user).size());
        Assert.assertEquals(moviesCountBeforeWish, movieControllerRest.getWishListMovies(user).size());
    }

    @Test
    public void rankMovieTest() {
        Movie movie = new Movie(2000, "Awesome movie!", "Interesting plot");
        movieControllerRest.addMovie(user, movie);

        movieControllerRest.rankMovie(user, movie.getId(), 5);
        Assert.assertEquals(5, movieControllerRest.getMovie(movie.getId()).getRank().getRank());
    }

    @Test
    public void addImageToMovieTest() throws IOException {
        Movie movie = new Movie(2000, "imaged movie!", "cool picture and some plot");
        movie = movieControllerRest.addMovie(user, movie);

        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream("src/test/resources/poster.jpg"));

        MultipartFile poster = new MockMultipartFile("Poster", IOUtils.toByteArray(bufferedInputStream));

        movieControllerRest.addImageToMovie(movie.getId(), poster);

        byte[] reachedPoster = movieControllerRest.getMovie(movie.getId()).getImage();
        Assert.assertNotNull(reachedPoster);
    }

    @Test
    public void searchMovieTest() {
        Movie movie = new Movie(2000, "searched movie!", "some plot");
        movie = movieControllerRest.addMovie(user, movie);

        List< Movie > foundMovies = movieControllerRest.searchMovie("searche");

        Assert.assertEquals(1, foundMovies.size());
    }

}
