package checkers;

import net.thumbtack.cinematheque.entity.listmovies.WishListMovie;
import net.thumbtack.cinematheque.exceptions.checkers.MovieChecker;
import net.thumbtack.cinematheque.exceptions.moviesexceptions.*;
import org.junit.Test;

public class MovieCheckerTest {
    @Test(expected = MovieNotFoundException.class)
    public void movieNotFoundTest() {
        MovieChecker.checkMovieFound(null);
    }

    @Test(expected = MovieNotFoundException.class)
    public void wishListMovieNotFoundTest() {
        MovieChecker.checkWishListMovie(null);
    }

    @Test(expected = MovieAlreadyInListException.class)
    public void movieAlreadyInListTest() {
        MovieChecker.checkNotInListMovie(new WishListMovie());
    }

    @Test(expected = OMDBImportException.class)
    public void omdbMovieNotFound() {
        MovieChecker.checkOMDBMovieData(null);
    }

    @Test(expected = ImageNotFoundException.class)
    public void checkMovieImage() {
        MovieChecker.checkMovieImageFound(new byte[ 0 ]);
    }

    @Test(expected = InvalidMovieException.class)
    public void checkMovieData() {
        MovieChecker.checkMovieData(null);
    }

}
