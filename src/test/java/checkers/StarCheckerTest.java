package checkers;

import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.exceptions.checkers.StarChecker;
import net.thumbtack.cinematheque.exceptions.starsexceptions.InvalidStarException;
import net.thumbtack.cinematheque.exceptions.starsexceptions.StarNotFoundException;
import org.junit.Test;

public class StarCheckerTest {

    @Test(expected = StarNotFoundException.class)
    public void starNotFoundTest() {
        StarChecker.checkStarFound(null);
    }

    @Test(expected = InvalidStarException.class)
    public void invalidStarDataTest() {
        StarChecker.checkStarData(new Star());
    }

}
