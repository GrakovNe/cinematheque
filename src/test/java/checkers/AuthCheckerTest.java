package checkers;

import net.thumbtack.cinematheque.exceptions.checkers.AuthChecker;
import net.thumbtack.cinematheque.exceptions.sessionexceptions.UnauthorizedException;
import org.junit.Test;

public class AuthCheckerTest {
    @Test(expected = UnauthorizedException.class)
    public void unauthorizedTest() {
        AuthChecker.checkAuth(null);
    }
}
