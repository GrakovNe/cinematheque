package checkers;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.exceptions.checkers.RanksChecker;
import net.thumbtack.cinematheque.exceptions.ranksexceptions.InvalidRankException;
import net.thumbtack.cinematheque.exceptions.ranksexceptions.MovieAlreadyRankedException;
import net.thumbtack.cinematheque.exceptions.ranksexceptions.RankNotFoundException;
import org.junit.Test;

public class RankCheckerTest {
    @Test(expected = MovieAlreadyRankedException.class)
    public void movieAlreadyRankedTest() {
        Movie movie = new Movie(1999, "The matrix", "Matrix everywhere!");
        movie.setId(1L);
        Rank rank = new Rank(movie, 5);
        movie.setRank(rank);
        RanksChecker.movieAlreadyRanked(movie);
    }

    @Test(expected = RankNotFoundException.class)
    public void rankNotFoundTest() {
        RanksChecker.checkRankFound(null);
    }

    @Test(expected = InvalidRankException.class)
    public void invalidRankData() {
        RanksChecker.checkRankData(new Rank());
    }
}
