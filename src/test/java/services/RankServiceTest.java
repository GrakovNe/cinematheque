package services;

import net.thumbtack.cinematheque.Application;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import net.thumbtack.cinematheque.repositories.RanksRepository;
import net.thumbtack.cinematheque.repositories.StarsRepository;
import net.thumbtack.cinematheque.services.MoviesService;
import net.thumbtack.cinematheque.services.RankService;
import net.thumbtack.cinematheque.services.StarService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ComponentScan({"net.thumbtack.cinematheque"})
public class RankServiceTest {
    @Autowired
    private MoviesRepository moviesRepository;

    @Autowired
    private MoviesService moviesService;

    @Autowired
    private RankService rankService;

    @Autowired
    private StarService starService;

    @Autowired
    private StarsRepository starsRepository;

    @Autowired
    private RanksRepository ranksRepository;


    @Before
    public void configureDB() {
        moviesRepository.deleteAll();
        starsRepository.deleteAll();
        ranksRepository.deleteAll();
    }

    @Test
    public void createRankTest() {
        Movie movie = new Movie(1966, "Movie-1", "Interesting plot");
        movie = moviesService.addMovie(movie);
        Rank rank = rankService.addRank(movie.getId(), 5);

        Assert.assertEquals(movie.getId(), rank.getMovieId());
        Assert.assertEquals(5, rank.getRank());
    }

    @Test
    public void addOrUpdateRankTest() {
        Movie movie = new Movie(1966, "Movie-2", "Interesting plot");
        movie = moviesService.addMovie(movie);
        rankService.addOrUpdateRank(movie.getId(), 4);
        Rank rank = moviesService.getMovieById(movie.getId()).getRank();

        Assert.assertEquals(movie.getId(), rank.getMovieId());
        Assert.assertEquals(4, rank.getRank());

        rankService.addOrUpdateRank(movie.getId(), 3);
        rank = moviesService.getMovieById(movie.getId()).getRank();

        Assert.assertEquals(movie.getId(), rank.getMovieId());
        Assert.assertEquals(3, rank.getRank());
    }

    @Test
    public void completelyUpdateRankTest() {
        Movie movie = new Movie(1966, "Movie-3", "Interesting plot");
        movie = moviesService.addMovie(movie);

        Rank rank = rankService.addRank(movie.getId(), 5);
        rank.setRank(4);

        rank = rankService.updateRank(rank.getId(), rank);
        Assert.assertEquals(4, rankService.getRankById(rank.getId()).getRank());
    }

    @Test
    public void nothingUpdateRankTest() {
        Movie movie = new Movie(1966, "Movie-4", "Interesting plot");
        movie = moviesService.addMovie(movie);

        Rank rank = rankService.addRank(movie.getId(), 5);
        rank.setRank(0);

        rank = rankService.updateRank(rank.getId(), rank);
        Assert.assertEquals(5, rankService.getRankById(rank.getId()).getRank());
    }

}
