package services;


import com.google.common.collect.Lists;
import net.thumbtack.cinematheque.Application;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.exceptions.checkers.MovieChecker;
import net.thumbtack.cinematheque.exceptions.moviesexceptions.MovieAlreadyExistException;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import net.thumbtack.cinematheque.repositories.StarsRepository;
import net.thumbtack.cinematheque.services.MoviesService;
import net.thumbtack.cinematheque.services.RankService;
import net.thumbtack.cinematheque.services.StarService;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ComponentScan({"net.thumbtack.cinematheque"})
public class MovieServiceTest {

    @Autowired
    private MoviesRepository moviesRepository;

    @Autowired
    private MoviesService moviesService;

    @Autowired
    private RankService rankService;

    @Autowired
    private StarService starService;

    @Autowired
    private StarsRepository starsRepository;

    @Before
    public void configureDB() {
        moviesRepository.deleteAll();
        starsRepository.deleteAll();
        Movie movie = new Movie(1966, "The good the bad and the ugly", "Interesting plot");
        moviesRepository.save(movie);
    }

    @Test
    public void createMovieTest() {
        int before = moviesService.getMovies().size();
        Movie movie = new Movie(1990, "Title", "Description");
        moviesService.addMovie(movie);
        Assert.assertEquals(before + 1, moviesService.getMovies().size());
    }

    @Test
    public void addPosterToMovieTest() throws IOException {
        Movie movie = new Movie(1999, "the matrix", "the matrix");
        Long movieId = moviesService.addMovie(movie).getId();

        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream("src/test/resources/poster.jpg"));
        moviesService.addPosterToMovie(movieId, IOUtils.toByteArray(bufferedInputStream));

        byte[] reachedMoviePoster = moviesService.getMovieById(movieId).getImage();
        Assert.assertNotNull(reachedMoviePoster);
    }

    @Test
    public void getMoviesOrderedByTitleTest() {
        moviesService.addMovie(new Movie(1999, "BBB", "Description"));
        moviesService.addMovie(new Movie(1999, "CCC", "Description"));
        moviesService.addMovie(new Movie(1999, "AAA", "Description"));

        List< Movie > orderedMovies = Lists.newArrayList(moviesService.getMoviesOrderedByTitle(1));

        Assert.assertEquals("AAA", orderedMovies.get(0).getTitle());
        Assert.assertEquals("BBB", orderedMovies.get(1).getTitle());
        Assert.assertEquals("CCC", orderedMovies.get(2).getTitle());
    }

    @Test
    public void getMoviesOrderedByYearTest() {
        moviesService.addMovie(new Movie(2000, "123", "Description"));
        moviesService.addMovie(new Movie(1988, "345", "Description"));
        moviesService.addMovie(new Movie(1999, "234", "Description"));

        List< Movie > orderedMovies = Lists.newArrayList(moviesService.getMoviesOrderedByYear(1));

        Assert.assertEquals(2000, orderedMovies.get(0).getYear());
        Assert.assertEquals(1999, orderedMovies.get(1).getYear());
        Assert.assertEquals(1988, orderedMovies.get(2).getYear());
    }

    @Test
    public void getMoviesOrderedByRankTest() {
        Movie movie;

        movie = moviesService.addMovie(new Movie(2000, "qwe", "Description"));
        rankService.addRank(movie.getId(), 1);

        movie = moviesService.addMovie(new Movie(1988, "wer", "Description"));
        rankService.addRank(movie.getId(), 5);

        movie = moviesService.addMovie(new Movie(1999, "ert", "Description"));
        rankService.addRank(movie.getId(), 3);

        List< Movie > orderedMovies = Lists.newArrayList(moviesService.getMoviesOrderedByRank(1));

        Assert.assertEquals(5, orderedMovies.get(0).getRank().getRank());
        Assert.assertEquals(3, orderedMovies.get(1).getRank().getRank());
        Assert.assertEquals(1, orderedMovies.get(2).getRank().getRank());
    }

    @Test
    public void addRankTest() {
        Star star = starService.addStar(new Star("John", "Smith"));
        Movie movie = moviesService.addMovie(new Movie(1900, "Movie-345", "Description"));

        int before = moviesService.getMovieById(movie.getId()).getStars().size();

        moviesService.addStar(movie.getId(), star.getId());
        Assert.assertEquals(before + 1, moviesService.getMovieById(movie.getId()).getStars().size());
    }

    @Test
    public void twiceAddStarTest() {
        Star star = starService.addStar(new Star("John-1", "Smith-1"));
        Movie movie = moviesService.addMovie(new Movie(1900, "Movie-3456", "Description"));

        int before = moviesService.getMovieById(movie.getId()).getStars().size();

        moviesService.addStar(movie.getId(), star.getId());
        Assert.assertEquals(before + 1, moviesService.getMovieById(movie.getId()).getStars().size());

        moviesService.addStar(movie.getId(), star.getId());
        moviesService.addStar(movie.getId(), star.getId());
        moviesService.addStar(movie.getId(), star.getId());
        moviesService.addStar(movie.getId(), star.getId());
        moviesService.addStar(movie.getId(), star.getId());

        Assert.assertEquals(before + 1, moviesService.getMovieById(movie.getId()).getStars().size());
    }

    @Test
    public void removeStarTest() {
        Star star = starService.addStar(new Star("John-3", "Smith-3"));
        Movie movie = moviesService.addMovie(new Movie(1900, "Movie-3467", "Description"));

        int before = moviesService.getMovieById(movie.getId()).getStars().size();

        moviesService.addStar(movie.getId(), star.getId());
        Assert.assertEquals(before + 1, moviesService.getMovieById(movie.getId()).getStars().size());

        moviesService.removeStar(movie.getId(), star.getId());
        Assert.assertEquals(before, moviesService.getMovieById(movie.getId()).getStars().size());
    }

    @Test
    public void deleteMovieTest() {
        int before = moviesService.getMovies().size();
        Movie movie = new Movie(1990, "Title-1", "Description-1");
        moviesService.addMovie(movie);
        Assert.assertEquals(before + 1, moviesService.getMovies().size());
        moviesService.deleteMovie(movie.getId());
        Assert.assertEquals(before, moviesService.getMovies().size());
    }

    @Test
    public void getWishListMoviesTest() {
        Long userId = 123L;

        Movie movie = new Movie(1990, "Title-2", "Description-2");
        movie = moviesService.addMovie(movie);

        int before = moviesService.getWishListMovies(userId).size();
        moviesService.addMovieToWishList(movie.getId(), userId);
        Assert.assertEquals(before + 1, moviesService.getWishListMovies(userId).size());
    }

    @Test
    public void getWishListMoviesOrderedByTitleTest() {
        Long userId = 123L;
        Movie movie;
        movie = moviesService.addMovie(new Movie(1999, "BBBWISH", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);

        movie = moviesService.addMovie(new Movie(1999, "CCCWISH", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);

        movie = moviesService.addMovie(new Movie(1999, "AAAWISH", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);

        List< Movie > wishMovies = Lists.newArrayList(moviesService.getWishListMoviesOrderedByTitle(1, userId));
        Assert.assertEquals("AAAWISH", wishMovies.get(0).getTitle());
        Assert.assertEquals("BBBWISH", wishMovies.get(1).getTitle());
        Assert.assertEquals("CCCWISH", wishMovies.get(2).getTitle());
    }

    @Test
    public void getWishListMoviesOrderedByYearTest() {
        Long userId = 123L;
        Movie movie;
        movie = moviesService.addMovie(new Movie(1999, "BBBWISH1", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);

        movie = moviesService.addMovie(new Movie(2005, "CCCWISH1", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);

        movie = moviesService.addMovie(new Movie(1955, "AAAWISH1", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);

        List< Movie > wishMovies = Lists.newArrayList(moviesService.getWishListMoviesOrderedByYear(1, userId));
        Assert.assertEquals(2005, wishMovies.get(0).getYear());
        Assert.assertEquals(1999, wishMovies.get(1).getYear());
        Assert.assertEquals(1955, wishMovies.get(2).getYear());
    }

    @Test
    public void getWishListMoviesOrderedByRankTest() {
        Long userId = 123L;
        Movie movie;
        movie = moviesService.addMovie(new Movie(1999, "BBBWISH2", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);
        rankService.addRank(movie.getId(), 3);

        movie = moviesService.addMovie(new Movie(2005, "CCCWISH2", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);
        rankService.addRank(movie.getId(), 1);

        movie = moviesService.addMovie(new Movie(1955, "AAAWISH2", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);
        rankService.addRank(movie.getId(), 5);

        List< Movie > wishMovies = Lists.newArrayList(moviesService.getWishListMoviesOrderedByRank(1, userId));
        Assert.assertEquals(5, wishMovies.get(0).getRank().getRank());
        Assert.assertEquals(3, wishMovies.get(1).getRank().getRank());
        Assert.assertEquals(1, wishMovies.get(2).getRank().getRank());
    }

    @Test
    public void getBookmarkListMoviesTest() {
        Long userId = 123L;

        Movie movie = new Movie(1990, "Title-3", "Description-3");
        movie = moviesService.addMovie(movie);

        int before = moviesService.getBookmarkListMovies(userId).size();
        moviesService.addMovieToBookmarkList(movie.getId(), userId);
        Assert.assertEquals(before + 1, moviesService.getBookmarkListMovies(userId).size());
    }

    @Test
    public void getBookmarkListMoviesOrderedByTitleTest() {
        Long userId = 123L;
        Movie movie;
        movie = moviesService.addMovie(new Movie(1999, "BBBBookmark", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);

        movie = moviesService.addMovie(new Movie(1999, "CCCBookmark", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);

        movie = moviesService.addMovie(new Movie(1999, "AAABookmark", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);

        List< Movie > BookmarkMovies = Lists.newArrayList(moviesService.getBookmarkListMoviesOrderedByTitle(1, userId));
        Assert.assertEquals("AAABookmark", BookmarkMovies.get(0).getTitle());
        Assert.assertEquals("BBBBookmark", BookmarkMovies.get(1).getTitle());
        Assert.assertEquals("CCCBookmark", BookmarkMovies.get(2).getTitle());
    }

    @Test
    public void getBookmarkListMoviesOrderedByYearTest() {
        Long userId = 123L;
        Movie movie;
        movie = moviesService.addMovie(new Movie(1999, "BBBBookmark1", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);

        movie = moviesService.addMovie(new Movie(2005, "CCCBookmark1", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);

        movie = moviesService.addMovie(new Movie(1955, "AAABookmark1", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);

        List< Movie > BookmarkMovies = Lists.newArrayList(moviesService.getBookmarkListMoviesOrderedByYear(1, userId));
        Assert.assertEquals(2005, BookmarkMovies.get(0).getYear());
        Assert.assertEquals(1999, BookmarkMovies.get(1).getYear());
        Assert.assertEquals(1955, BookmarkMovies.get(2).getYear());
    }

    @Test
    public void getBookmarkListMoviesOrderedByRankTest() {
        Long userId = 123L;
        Movie movie;
        movie = moviesService.addMovie(new Movie(1999, "BBBBookmark2", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);
        rankService.addRank(movie.getId(), 3);

        movie = moviesService.addMovie(new Movie(2005, "CCCBookmark2", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);
        rankService.addRank(movie.getId(), 1);

        movie = moviesService.addMovie(new Movie(1955, "AAABookmark2", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);
        rankService.addRank(movie.getId(), 5);

        List< Movie > BookmarkMovies = Lists.newArrayList(moviesService.getBookmarkListMoviesOrderedByRank(1, userId));
        Assert.assertEquals(5, BookmarkMovies.get(0).getRank().getRank());
        Assert.assertEquals(3, BookmarkMovies.get(1).getRank().getRank());
        Assert.assertEquals(1, BookmarkMovies.get(2).getRank().getRank());
    }

    @Test
    public void completelyUpdateMovie() throws IOException {
        Movie movie = new Movie(1990, "TitleOld", "DescriptionOld");
        movie = moviesService.addMovie(movie);

        movie.setTitle("TitleNew");
        movie.setDescription("DescriptionNew");
        movie.setYear(1991);

        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream("src/test/resources/poster.jpg"));
        movie.setImage(IOUtils.toByteArray(bufferedInputStream));

        movie = moviesService.updateMovie(movie.getId(), movie);

        Assert.assertEquals(1991, movie.getYear());
        Assert.assertEquals("TitleNew", movie.getTitle());
        Assert.assertEquals("DescriptionNew", movie.getDescription());
    }

    @Test
    public void nothingUpdateMovie() {
        Movie movie = new Movie(1990, "TitleOld-1", "DescriptionOld-1");
        movie = moviesService.addMovie(movie);

        movie.setTitle("");
        movie.setDescription("");
        movie.setYear(0);

        movie = moviesService.updateMovie(movie.getId(), movie);

        Assert.assertEquals(1990, movie.getYear());
        Assert.assertEquals("TitleOld-1", movie.getTitle());
        Assert.assertEquals("DescriptionOld-1", movie.getDescription());
    }

    @Test(expected = MovieAlreadyExistException.class)
    public void createMovieTwiceTest() {
        int before = moviesService.getMovies().size();
        Movie movie = new Movie(1990, "Title-5", "Description-5");
        moviesService.addMovie(movie);
        Assert.assertEquals(before + 1, moviesService.getMovies().size());
        moviesService.addMovie(movie);
    }

    @Test
    public void isMovieInWishList() {
        Long userId = 12345L;
        Movie movie = moviesService.addMovie(new Movie(1900, "LikedMovie", "Description"));
        moviesService.addMovieToWishList(movie.getId(), userId);
        Assert.assertTrue(moviesService.isMovieInWishList(movie.getId(), userId));
        Assert.assertFalse(moviesService.isMovieInWishList(movie.getId(), userId + 1));
    }

    @Test
    public void isMovieInBookmarkList() {
        Long userId = 12345L;
        Movie movie = moviesService.addMovie(new Movie(1900, "BookmarkedMovie", "Description"));
        moviesService.addMovieToBookmarkList(movie.getId(), userId);
        Assert.assertTrue(moviesService.isMovieInBookmarkList(movie.getId(), userId));
        Assert.assertFalse(moviesService.isMovieInBookmarkList(movie.getId(), userId + 1));
    }

    @Test
    public void searchMovieTest() {
        List< Movie > foundFirstlyMovies = moviesService.cascadeSearchMovie("1966");
        List< Movie > foundSecondaryMovies = moviesService.cascadeSearchMovie("The good");
        Assert.assertEquals(foundFirstlyMovies, foundSecondaryMovies);
    }

    @Test
    public void importFromOmdbTest() {
        Movie movie = moviesService.importMovieFromOMBD("Alien");
        MovieChecker.checkMovieData(movie);
    }

    @Test
    public void isMovieExistsInOmdb() {
        Assert.assertTrue(moviesService.isMovieExistsInOmdb("The matrix"));
        Assert.assertFalse(moviesService.isMovieExistsInOmdb("GiveMe404OnMovieRequest!!!"));
    }

    @Test
    public void getTopMoviesTest() {
        Movie movie = moviesService.addMovie(new Movie(1899, "Top movie", "Cool!"));
        moviesService.addMovieToWishList(movie.getId(), 123L);
        moviesService.addMovieToWishList(movie.getId(), 124L);
        moviesService.addMovieToWishList(movie.getId(), 125L);
        List< Movie > topMovies = moviesService.getTopMovies();
        Assert.assertEquals(movie, topMovies.get(0));
    }

    @Test
    public void getRecentMovies() {
        Movie movie = moviesService.addMovie(new Movie(1899, "New Movie", "Cool!"));
        List< Movie > topMovies = moviesService.getRecentMovies();
        Assert.assertEquals(movie, topMovies.get(0));
    }

}
