package services;

import com.google.common.collect.Lists;
import net.thumbtack.cinematheque.Application;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.exceptions.starsexceptions.StarAlreadyExistsException;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import net.thumbtack.cinematheque.repositories.StarsRepository;
import net.thumbtack.cinematheque.services.MoviesService;
import net.thumbtack.cinematheque.services.RankService;
import net.thumbtack.cinematheque.services.StarService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ComponentScan({"net.thumbtack.cinematheque"})
public class StarServiceTest {
    @Autowired
    private MoviesRepository moviesRepository;

    @Autowired
    private MoviesService moviesService;

    @Autowired
    private RankService rankService;

    @Autowired
    private StarService starService;

    @Autowired
    private StarsRepository starsRepository;

    private Movie movie;

    @Before
    public void configureDB() {
        moviesRepository.deleteAll();
        starsRepository.deleteAll();
        movie = new Movie(1966, "The good the bad and the ugly", "Interesting plot");
        movie = moviesService.addMovie(movie);
    }

    @Test
    public void createStarTest() {
        int before = starService.getStars().size();
        Star star = starService.addStar(new Star("John", "Smith"));
        Assert.assertEquals(before + 1, starService.getStars().size());
    }

    @Test(expected = StarAlreadyExistsException.class)
    public void createTwiceStarTest() {
        starService.addStar(new Star("John-1", "Smith-1"));
        starService.addStar(new Star("John-1", "Smith-1"));
    }

    @Test
    public void deleteStarTest() {
        int before = starService.getStars().size();
        Star star = starService.addStar(new Star("John-2", "Smith-2"));
        starService.deleteStar(star.getId());
        Assert.assertEquals(before, starService.getStars().size());
    }

    @Test
    public void getPageStarsTest() {
        int before = starService.getStars().size();
        starService.addStar(new Star("John-3", "Smith-3"));
        List pagedStars = Lists.newArrayList(starService.getStars(1));
        Assert.assertEquals(before + 1, pagedStars.size());
    }

    @Test
    public void getStarByIdTest() {
        Star star = starService.addStar(new Star("John-4", "Smith-4"));
        Star reachedStar = starService.getStarById(star.getId());
        Assert.assertEquals(star, reachedStar);
    }

    @Test
    public void getStarByLastNameTest() {
        Star star = starService.addStar(new Star("John-5", "Smith-5"));
        List< Star > reachedStar = starService.getStarsByLastName(star.getLastName());
        Assert.assertEquals(star, reachedStar.get(0));
    }

    @Test
    public void getStarByFirstNameTest() {
        Star star = starService.addStar(new Star("John-6", "Smith-6"));
        List< Star > reachedStar = starService.getByFirstName(star.getFirstName());
        Assert.assertEquals(star, reachedStar.get(0));
    }

    @Test
    public void completelyUpdateStarTest() {
        Star star = starService.addStar(new Star("John-7", "Jack-7", "Smith-7"));

        star.setLastName("newSmith");
        star.setFirstName("newJohn");
        star.setMiddleName("newJack");

        Star updatedStar = starService.updateStar(star.getId(), star);
        Assert.assertEquals("newSmith", updatedStar.getLastName());
        Assert.assertEquals("newJohn", updatedStar.getFirstName());
        Assert.assertEquals("newJack", updatedStar.getMiddleName());
    }

    @Test
    public void nothingUpdateStarTest() {
        Star star = starService.addStar(new Star("John-7", "Jack-7", "Smith-7"));

        star.setLastName("");
        star.setFirstName("");
        star.setMiddleName("");

        Star updatedStar = starService.updateStar(star.getId(), star);
        Assert.assertEquals("Smith-7", updatedStar.getLastName());
        Assert.assertEquals("John-7", updatedStar.getFirstName());
        Assert.assertEquals("Jack-7", updatedStar.getMiddleName());
    }

    @Test
    public void searchStarByLastName() {
        Star star = starService.addStar(new Star("John-8", "Jack-8", "Smith-8"));
        List< Star > foundStarsFirst = starService.cascadeSearchStar("Jack-8");
        List< Star > foundStarsSecond = starService.cascadeSearchStar("John-8");
        List< Star > foundStarsThird = starService.cascadeSearchStar("Smith-8");

        Assert.assertEquals(foundStarsFirst, foundStarsSecond);
        Assert.assertEquals(foundStarsFirst, foundStarsThird);
        Assert.assertEquals(foundStarsSecond, foundStarsThird);
        Assert.assertEquals(star, foundStarsFirst.get(0));
    }

}
