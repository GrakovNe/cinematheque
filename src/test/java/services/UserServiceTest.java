package services;


import net.thumbtack.cinematheque.Application;
import net.thumbtack.cinematheque.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ComponentScan({"net.thumbtack.cinematheque"})
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Test
    public void getUserRoleForRandomUser() {
        Long randomUserID = 198489498189198198L; // guarantee random

        List< String > roles = userService.getOauthUserRoles(randomUserID);

        Assert.assertTrue(roles.contains("ROLE_USER"));
        Assert.assertFalse(roles.contains("ROLE_ADMIN"));
    }

    @Test
    public void getUserRoleForAdmin() {
        Long adminID = 1096619753760209L;
        List< String > roles = userService.getOauthUserRoles(adminID);

        Assert.assertTrue(roles.contains("ROLE_USER"));
        Assert.assertTrue(roles.contains("ROLE_ADMIN"));
    }
}
