package configuration;

import net.thumbtack.cinematheque.Application;
import net.thumbtack.cinematheque.config.ApplicationConfigurationProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ComponentScan({"net.thumbtack.cinematheque"})
public class ApplicationConfigProviderTest {

    @Autowired
    ApplicationConfigurationProvider configurationProvider;

    @Test
    public void getPageSizeTest() {
        int pageSize = configurationProvider.getPageSize();
        Assert.assertNotEquals(0, pageSize);
    }

    @Test
    public void getDefaultRoleMemberTest() {
        String defaultRoleMember = configurationProvider.getDefaultMemberRole();
        Assert.assertNotNull(defaultRoleMember);
    }

    @Test
    public void getAdminRoleMemberTest() {
        String adminRoleMember = configurationProvider.getAdminMemberRole();
        Assert.assertNotNull(adminRoleMember);
    }

    @Test
    public void getMaxPosterSizeTest() {
        int maxPosterSize = configurationProvider.getMaxPosterSize();
        Assert.assertNotEquals(0, maxPosterSize);
    }

    @Test
    public void getAdminsListTest() {
        List adminsList = configurationProvider.getAdmins();
        Assert.assertNotNull(adminsList);
    }

    @Test
    public void isUserIsAdminTest() {
        @SuppressWarnings("unchecked")
        List< Long > adminsList = configurationProvider.getAdmins();
        Assert.assertTrue(configurationProvider.isUserIsAdmin(adminsList.get(0)));
        Assert.assertFalse(configurationProvider.isUserIsAdmin(adminsList.get(0) + 1));
    }

}
