package datavalidation;

import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.exceptions.checkers.StarChecker;
import net.thumbtack.cinematheque.exceptions.starsexceptions.InvalidStarException;
import org.junit.Test;

public class StarDataCheckTest {

    @Test
    public void starValidationTest() {
        Star star = new Star("FirstName", "LastName");
        StarChecker.checkStarData(star);
    }

    @Test(expected = InvalidStarException.class)
    public void invalidStarFirstNameTest() {
        Star star = new Star("", "LastName");
        StarChecker.checkStarData(star);
    }

    @Test(expected = InvalidStarException.class)
    public void invalidStarLastNameTest() {
        Star star = new Star("", "LastName");
        StarChecker.checkStarData(star);
    }


}
