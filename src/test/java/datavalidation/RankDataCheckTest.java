package datavalidation;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.exceptions.checkers.RanksChecker;
import net.thumbtack.cinematheque.exceptions.ranksexceptions.InvalidRankException;
import org.junit.Before;
import org.junit.Test;

public class RankDataCheckTest {

    private Movie movie = new Movie(1999, "The matrix", "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.");

    @Before
    public void setMovieId() {
        movie.setId(1L);
    }

    @Test
    public void rankValidationTest() {
        Rank rank = new Rank(movie, 5);
        RanksChecker.checkRankData(rank);
    }

    @Test(expected = InvalidRankException.class)
    public void lessMinimalRankTest() {
        Rank rank = new Rank(movie, 0);
        RanksChecker.checkRankData(rank);
    }

    @Test(expected = InvalidRankException.class)
    public void greaterMaximalRankTest() {
        Rank rank = new Rank(movie, 9_000_000);
        RanksChecker.checkRankData(rank);
    }
}
