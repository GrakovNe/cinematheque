package datavalidation;

import net.thumbtack.cinematheque.exceptions.checkers.AuthChecker;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthDataCheckTest {

    private OAuth2Authentication user;

    @Before
    public void authUser() {
        OAuth2Request clientAuthentication = new OAuth2Request(null, null, null, true, null, null, null, null, null);
        Authentication userAuthentication = mock(Authentication.class);
        Map< String, String > userData = new HashMap<>();
        userData.put("id", "1234567890");
        userData.put("name", "JUnitUser");
        when(userAuthentication.getDetails()).thenReturn(userData);

        user = new OAuth2Authentication(clientAuthentication, userAuthentication);
    }

    @Test
    public void authUserTest() {
        AuthChecker.checkAuth(user);
    }

}
