package datavalidation;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.exceptions.checkers.MovieChecker;
import net.thumbtack.cinematheque.exceptions.moviesexceptions.InvalidMovieException;
import org.junit.Test;


public class MovieDataCheckTest {
    @Test
    public void movieValidation() {
        Movie movie = new Movie(1999, "The matrix", "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.");
        MovieChecker.checkMovieData(movie);
    }

    @Test(expected = InvalidMovieException.class)
    public void InvalidYearMovieDataTest() {
        Movie movie = new Movie(123, "123", "123123");
        MovieChecker.checkMovieData(movie);
    }

    @Test(expected = InvalidMovieException.class)
    public void InvalidTitleMovieDataTest() {
        Movie movie = new Movie(1990, "", "123123");
        MovieChecker.checkMovieData(movie);
    }
}
