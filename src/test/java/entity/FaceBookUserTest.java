package entity;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class FaceBookUserTest {
    private static OAuth2Authentication user;
    private static OAuth2Request clientAuthentication;

    @BeforeClass
    public static void mockAuth() {
        Authentication userAuthentication = mock(Authentication.class);
        Map< String, String > userData = new HashMap<>();
        userData.put("id", "1234567890");
        userData.put("name", "JUnitUser");
        when(userAuthentication.getDetails()).thenReturn(userData);

        user = new OAuth2Authentication(clientAuthentication, userAuthentication);
    }

    @Test
    public void faceBookUserDataTest() {

        net.thumbtack.cinematheque.entity.FaceBookUser faceBookUser = new net.thumbtack.cinematheque.entity.FaceBookUser(user);

        Assert.assertEquals("JUnitUser", faceBookUser.getRealName());
        Assert.assertEquals(Long.valueOf("1234567890"), faceBookUser.getId());
        Assert.assertEquals("1234567890", faceBookUser.getUserName());

        Assert.assertNotNull(faceBookUser.toString());
    }
}
