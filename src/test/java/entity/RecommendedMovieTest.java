package entity;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.listmovies.RecommendedListMovie;
import org.junit.Assert;
import org.junit.Test;

public class RecommendedMovieTest {
    @Test
    public void recommendedMovieTest(){
        Movie movie = new Movie(1900, "Movie", "Description");
        movie.setId(1L);
        RecommendedListMovie recommendedListMovie = new RecommendedListMovie(movie.getId(), 123L);
        Assert.assertEquals(movie.getId(), recommendedListMovie.getMovieId());
    }
}
