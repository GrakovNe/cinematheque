package entity;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.listmovies.BookmarkListMovie;
import org.junit.Assert;
import org.junit.Test;

public class BookmarkListMovieTest {

    @Test
    public void BookmarkListMovieTest() {
        Movie movie = new Movie(1900, "Movie", "Description");
        movie.setId(1L);
        BookmarkListMovie bookmarkListMovie = new BookmarkListMovie(movie.getId(), 1L);
        Assert.assertNotNull(bookmarkListMovie.getMovieId());
    }
}
