package entity;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.entity.Star;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class MovieTest {

    @Test
    public void moviePrimaryDataTest() {
        Movie movie = new Movie(1990, "Movie", "Movie description");

        Assert.assertNotNull(movie.getYear());
        Assert.assertNotNull(movie.getTitle());
        Assert.assertNotNull(movie.getDescription());
    }

    @Test
    public void movieSecondaryDataTest() {
        Movie movie = new Movie(1990, "Movie", "Movie description");
        Rank rank = new Rank(movie, 5);
        movie.setRank(rank);

        Assert.assertNotNull(movie.getRank());
        Assert.assertEquals(5, movie.getRank().getRank());

        Set< Star > starSet = new HashSet<>();
        starSet.add(new Star("FirstName-1", "LastName-1"));
        starSet.add(new Star("FirstName-2", "LastName-2"));
        starSet.add(new Star("FirstName-3", "LastName-3"));
        movie.setStars(starSet);

        Assert.assertNotNull(movie.getStars());
        Assert.assertEquals(3, movie.getStars().size());
    }

    @Test
    public void movieEqualsTest() {
        Movie firstMovie = new Movie(1990, "Movie", "Movie description");
        Movie secondMovie = new Movie(1990, "Movie", "Movie description");

        Assert.assertEquals(secondMovie, firstMovie);
        Assert.assertEquals(firstMovie.hashCode(), secondMovie.hashCode());

        secondMovie = new Movie(1995, "Movie-1", "Movie-1 description");

        Assert.assertNotEquals(secondMovie, firstMovie);
        Assert.assertNotEquals(firstMovie.hashCode(), secondMovie.hashCode());

    }

    @Test
    public void movieStringDataTest() {
        Movie firstMovie = new Movie(1990, "Movie", "Movie description");
        String movieData = firstMovie.toString();
        Assert.assertNotNull(movieData);
    }

}
