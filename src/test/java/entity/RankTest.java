package entity;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import org.junit.Assert;
import org.junit.Test;

public class RankTest {

    @Test
    public void primaryRankDataTest() {
        Movie movie = new Movie(1990, "Movie", "Description");
        movie.setId(1L);

        Rank rank = new Rank(movie, 5);
        rank.setId(1L);

        Assert.assertEquals(5, rank.getRank());
        Assert.assertNotNull(rank.getId());
        Assert.assertNotNull(rank.getMovieId());
    }

    @Test
    public void equalsTest() {
        Movie movie = new Movie(1990, "Movie", "Description");
        Rank firstRank = new Rank(movie, 5);
        Rank secondRank = new Rank(movie, 5);

        Assert.assertTrue(firstRank.equals(secondRank));
        Assert.assertEquals(firstRank.hashCode(), secondRank.hashCode());

        secondRank.setRank(1);

        Assert.assertFalse(firstRank.equals(secondRank));
        Assert.assertNotEquals(firstRank.hashCode(), secondRank.hashCode());
    }

    @Test
    public void starDataTest() {
        Movie movie = new Movie(1990, "Movie", "Description");
        Rank firstRank = new Rank(movie, 5);

        Assert.assertNotNull(firstRank.toString());
    }
}
