package entity;

import net.thumbtack.cinematheque.entity.Star;
import org.junit.Assert;
import org.junit.Test;

public class StarTest {
    @Test
    public void starPrimaryDataTest() {
        Star star = new Star("John", "Mike", "Smith");

        Assert.assertNotNull(star.getFirstName());
        Assert.assertNotNull(star.getMiddleName());
        Assert.assertNotNull(star.getLastName());

        star = new Star("John", "Smith");

        Assert.assertNotNull(star.getFirstName());
        Assert.assertNull(star.getMiddleName());
        Assert.assertNotNull(star.getLastName());

        star.setId(1L);

        Assert.assertNotNull(star.getId());
    }

    @Test
    public void EqualsStarTest() {
        Star firstStar = new Star("John", "Mike", "Smith");
        Star secondStar = new Star("John", "Mike", "Smith");

        Assert.assertTrue(firstStar.equals(secondStar));
        Assert.assertEquals(firstStar.hashCode(), secondStar.hashCode());

        secondStar = new Star("Mike", "Smith", "John");

        Assert.assertFalse(firstStar.equals(secondStar));
        Assert.assertNotEquals(firstStar.hashCode(), secondStar.hashCode());
    }

    @Test
    public void starDataStringTest() {
        Star firstStar = new Star("John", "Mike", "Smith");
        Assert.assertNotNull(firstStar.toString());
    }
}
