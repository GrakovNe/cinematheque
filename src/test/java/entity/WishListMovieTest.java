package entity;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.listmovies.WishListMovie;
import org.junit.Assert;
import org.junit.Test;

public class WishListMovieTest {

    @Test
    public void BookmarkListMovieTest() {
        Movie movie = new Movie(1900, "Movie", "Description");
        movie.setId(1L);
        WishListMovie wishListMovie = new WishListMovie(movie.getId(), 1L);
        Assert.assertNotNull(wishListMovie.getMovieId());
    }
}
