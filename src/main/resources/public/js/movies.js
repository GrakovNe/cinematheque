function checkFileSize(fileSize){
    return (!(fileSize > 4 * 1024 * 1024));
}

function saveMovie(){
    var movieData = {
        title       :  $('#title').val(),
        year        :   $('#year').val(),
        description : $('#description').val()
    };

    var formData = new FormData($("#upload-file-form")[0]);
    formData.append("movie", JSON.stringify(movieData));

    if (!checkFileSize(formData.get('poster').size)){
        $("#error-when-processing-movie").html("Too large file");
        $("#error-when-processing-movie").show();
        return;
    }

    $.ajax({
       url: "/movie/",
       type: "POST",
       data: formData,
       enctype: 'multipart/form-data',
       processData: false,
       contentType: false,
       cache: false,
       complete: function (xhr, textStatus) {
        if (xhr.status == 200){
                window.location.href = "/movie";
        } else {
                //$("#error-when-processing-movie").html(JSON.parse(xhr.responseText).message);
                $("#error-when-processing-movie").show();
            }
      }
    });

};

function loadImage(movieId){
    $.ajax({
        type: "GET",
        url: "/movie/" + movieId + "/image",
        complete: function (xhr, textStatus) {
            if (xhr.status == 200){
               $("#movie-image").attr("src", "http://" + window.location.host + "/movie/" + movieId +"/image");
               $("#movie-image-big").attr("src", "http://" + window.location.host + "/movie/" + movieId +"/image");

                $('#image-modal').on('shbookmark.bs.modal', function () {
                    $(this).find('.modal-dialog').css({width:'80%', height:'80%'});
                });

            } else {
               console.log("image not found");
            }
        }
    });

     $("#movie-image").removeAttr("onload");
}

function saveMovieFromOMBD(){
        var movieTitle = $('#omdb-title').val();


    $.ajax({
        type: "POST",
        url: "/movie/import/?title=" + movieTitle,
        contentType: "application/json",
        async: false,
        complete: function (xhr, textStatus) {
            if (xhr.status == 200){
                window.location.href = "/movie";
            } else {
                $("#error-when-processing-movie").show();
                $("#error-when-processing-movie").html(JSON.parse(xhr.responseText).message);
            }
        },
        dataType: "json"
    });

};

function updateMovie(movieId){
    var movieData = {
            title       :  $('#title').val(),
            year        :   $('#year').val(),
            description : $('#description').val()
        };

        var formData = new FormData($("#upload-file-form")[0]);
        formData.append("movie", JSON.stringify(movieData));

          if (!checkFileSize(formData.get('poster').size)){
                $("#error-when-processing-movie").html("Too large file");
                $("#error-when-processing-movie").show();
                return;
          }

        $.ajax({
           type: "POST",
           url: "/movie/" + movieId,
           data: formData,
           enctype: 'multipart/form-data',
           processData: false,
           contentType: false,
           cache: false
        });

        window.location.href = "/movie/" + movieId;
};

function rankMovie(movieId ,value){
        $.ajax({
            type: "PUT",
            url: "/movie/" + movieId + "/rank/" + "?value=" + value,
            contentType: "application/json",
            dataType: "json",
            async: false
        });
    location.reload();
};

function removeStar(movieId, starId){
        console.log(starId);

        $.ajax({
            type: "DELETE",
            url: "/movie/" + movieId + "/star/" + starId,
            contentType: "application/json",
            dataType: "json",
            async: false
        });

        window.location.href = "/movie/" + movieId;
};

function addToWishList(movieId){
    $.ajax({
            type: "POST",
            url: "/movie/" + movieId + "/wishlist",
            contentType: "application/json",
            dataType: "json",
            async: false
        });

        window.location.href = "/movie/" + movieId;
    }

function addToBookmarkList(movieId){
    $.ajax({
            type: "POST",
            url: "/movie/" + movieId + "/bookmarklist",
            contentType: "application/json",
            dataType: "json",
            async: false
        });

        window.location.href = "/movie/" + movieId;
    }

function removeFromWishList(movieId){
    $.ajax({
            type: "DELETE",
            url: "/movie/" + movieId + "/wishlist",
            contentType: "application/json",
            dataType: "json",
            async: false
        });

        window.location.href = "/movie/" + movieId;
    }

function removeFromBookmarkList(movieId){
    $.ajax({
            type: "DELETE",
            url: "/movie/" + movieId + "/bookmarklist",
            contentType: "application/json",
            dataType: "json",
            async: false
        });

        window.location.href = "/movie/" + movieId;
};

function deleteMovie(movieId){
        $.ajax({
        type: "DELETE",
        url: "/movie/" + movieId,
        contentType: "application/json",
        async: false,
        dataType: "json"
        });

        location.reload();
};

function searchMovie(){
    var searchData = $('#search-form').val();
    window.location.href = "/movie/search?searchPhrase=" + searchData;
    console.log("OK");
};

