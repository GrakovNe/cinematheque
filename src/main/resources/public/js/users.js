function signUp(){
        var userData = {
            userName:  $('#username').val(),
            realName:  $('#realname').val(),
            password:  CryptoJS.SHA256($('#password').val()).toString()
        };

         $.ajax({
        type: "POST",
        data: JSON.stringify(userData),
        url: "/register",
        contentType: "application/json",
        async: false,
        complete: function (xhr, textStatus) {
            if (xhr.status == 200){
                window.location.href = "/";
            } else {
                $("#error-when-processing-register").html(JSON.parse(xhr.responseText).message);
                $("#error-when-processing-register").show();
            }
        },
        dataType: "json"
    });
}

function editUserData(){
        var userData = {
            realName:  $('#realname').val()
        };

        console.log(JSON.stringify(userData));

        $.ajax({
                type: "POST",
                data: JSON.stringify(userData),
                url: "/profile",
                contentType: "application/json",
                async: false,
                dataType: "json"
            });

        window.location.href = "/logout";
}