$("#search-form").keyup(
    function (e) {
        if (e.keyCode == 13) {
            searchMovie();
        }
    }
);

$("#omdb-title").keyup(
    function (e) {
        if (e.keyCode == 13) {
            saveMovieFromOMBD();
        }
    }
);