function saveStar(){
        var starData = {
        firstName:  $('#firstName').val(),
        middleName:   $('#middleName').val(),
        lastName: $('#lastName').val()
    };

    $.ajax({
        type: "POST",
        url: "/star/",
        data: JSON.stringify(starData),
        contentType: "application/json",
        async: false,
        complete: function (xhr, textStatus) {
                    if (xhr.status == 200){
                        window.location.href = "/star";
                    } else {
                        $("#error-when-processing-star").html(JSON.parse(xhr.responseText).message);
                        $("#error-when-processing-star").show();
                    }
                },
        dataType: "json"
    });
};

function attachStar(movieId){
        var starId =  $('#starList').val();
        console.log(starId);

        $.ajax({
            type: "POST",
            url: "/movie/" + movieId + "/star/" + starId,
            contentType: "application/json",
            dataType: "json",
            async: false
        });

        window.location.href = "/movie/" + movieId;
};

function updateStar(starId){
        var starData = {
        firstName:  $('#firstName').val(),
        middleName:   $('#middleName').val(),
        lastName:   $('#lastName').val()
    };


    $.ajax({
        type: "POST",
        url: "/star/" + starId,
        data: JSON.stringify(starData),
        contentType: "application/json",
        async: false,
        dataType: "json"
    });

    window.location.href = "/star/";
};

function deleteStar(starId){
        $.ajax({
        type: "DELETE",
        url: "/star/" + starId,
        contentType: "application/json",
        async: false,
        dataType: "json"
        });

        location.reload();
};

function searchStar(){
    var searchData = $('#search-form-star').val();
    window.location.href = "/star/search?searchPhrase=" + searchData;
};