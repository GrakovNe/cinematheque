CREATE TABLE movies_wishlist
(
  id serial NOT NULL,
  movie_id integer,
  user_id integer NOT NULL,
  CONSTRAINT movies_wishlist_pkey PRIMARY KEY (id),
  CONSTRAINT movies_wishlist_movie_id_fkey FOREIGN KEY (movie_id)
      REFERENCES movies (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT movies_wishlist_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);
