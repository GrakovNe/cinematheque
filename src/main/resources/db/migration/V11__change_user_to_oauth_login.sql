ALTER TABLE movies_wishlist DROP COLUMN user_id;
ALTER TABLE movies_owned DROP COLUMN user_id;

ALTER TABLE movies_owned add COLUMN user_id bigint;
ALTER TABLE movies_wishlist add COLUMN user_id bigint;

drop table user_roles;
drop table users;