CREATE TABLE stars
(
  id serial NOT NULL,
  first_name text NOT NULL,
  middle_name text,
  last_name text NOT NULL,
  CONSTRAINT stars_pkey PRIMARY KEY (id)
);