CREATE TABLE movies_stars
(
  id serial NOT NULL,
  movie_id integer,
  star_id integer,
  CONSTRAINT movies_stars_pkey PRIMARY KEY (id),
  CONSTRAINT movies_fkey FOREIGN KEY (movie_id)
      REFERENCES movies (id) MATCH SIMPLE
      ON UPDATE SET DEFAULT ON DELETE CASCADE,
  CONSTRAINT movies_stars_star_id_fkey FOREIGN KEY (star_id)
      REFERENCES stars (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);