CREATE TABLE users
(
  id serial NOT NULL,
  user_name text NOT NULL,
  password text NOT NULL,
  enabled boolean,
  real_name text,
  CONSTRAINT users_pkey PRIMARY KEY (id)
);