CREATE TABLE movies_recommended
(
  id serial NOT NULL,
  movie_id integer,
  user_id bigint NOT NULL,
  CONSTRAINT movies_recommend PRIMARY KEY (id),
  CONSTRAINT movies_recommend_movie_id_fkey FOREIGN KEY (movie_id)
      REFERENCES movies (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);
