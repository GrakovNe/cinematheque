CREATE TABLE ranks
(
  id serial NOT NULL,
  movie_id integer NOT NULL,
  rank integer,
  CONSTRAINT ranks_pkey PRIMARY KEY (id),
  CONSTRAINT ranks_movie_id_fkey FOREIGN KEY (movie_id)
      REFERENCES movies (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT ranks_movie_id_key UNIQUE (movie_id)
);