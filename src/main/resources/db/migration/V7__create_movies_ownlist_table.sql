CREATE TABLE movies_owned
(
  id serial NOT NULL,
  movie_id integer,
  user_id integer NOT NULL,
  CONSTRAINT movies_owned_pkey PRIMARY KEY (id),
  CONSTRAINT movies_owned_movie_id_fkey FOREIGN KEY (movie_id)
      REFERENCES movies (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT movies_owned_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);