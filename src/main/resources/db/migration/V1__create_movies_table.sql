CREATE TABLE movies (
  id serial NOT NULL,
  title text,
  year integer,
  image bigint,
  CONSTRAINT movies_pkey PRIMARY KEY (id)
);