package net.thumbtack.cinematheque.config;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Spring security configuration class
 */

@Configuration
@EnableOAuth2Sso
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Spring security configuration
     *
     * @param http Spring http security
     * @throws Exception if any broken
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests().antMatchers("/").permitAll().and()
                .authorizeRequests().antMatchers("/images/**").permitAll().and()
                .authorizeRequests().antMatchers("/movie/**").permitAll().and()
                .authorizeRequests().antMatchers("/star/**").permitAll().and()
                .authorizeRequests().antMatchers("/api/**").permitAll().and()
                .authorizeRequests().antMatchers("/media/**").permitAll().and()
                .authorizeRequests().antMatchers("/register**").permitAll().and()
                .authorizeRequests().antMatchers("/favicon.ico").permitAll().and()
                .authorizeRequests().antMatchers("/css/**").permitAll().and()
                .authorizeRequests().antMatchers("/js/**").permitAll().and()
                .formLogin().loginPage("/login").permitAll().and()
                .authorizeRequests().anyRequest().authenticated();
    }
}
