package net.thumbtack.cinematheque.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Configuration provider from application properties file
 */

@Component
@ConfigurationProperties(prefix = "spring.config", locations = "application.yml")
public class ApplicationConfigurationProvider {
    private Integer pageSize;
    private String defaultMemberRole;
    private String adminMemberRole;
    private Integer maxPosterSize;
    private List admins;

    /**
     * Getter for page size
     *
     * @return number of elements at one page
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * Setter for pageSize. It needs for Spring
     *
     * @param pageSize number of elements at page (no needs to think about it)
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Getter for default role of user.
     *
     * @return String with default role
     */
    public String getDefaultMemberRole() {
        return defaultMemberRole;
    }

    /**
     * Setter for default role of user. It needs to Spring
     *
     * @param defaultMemberRole String with default role (no needs to think about it)
     */
    public void setDefaultMemberRole(String defaultMemberRole) {
        this.defaultMemberRole = defaultMemberRole;
    }

    /**
     * Getter for maximal size of poster
     *
     * @return integer with size
     */
    public Integer getMaxPosterSize() {
        return maxPosterSize;
    }

    /**
     * Setter for max poster size. It needs to Spring
     *
     * @param maxPosterSize integer with size (no needs to think about it)
     */
    public void setMaxPosterSize(Integer maxPosterSize) {
        this.maxPosterSize = maxPosterSize;
    }

    /**
     * Getter for admin member role
     *
     * @return String with admin role
     */
    public String getAdminMemberRole() {
        return adminMemberRole;
    }

    /**
     * Setter for admin member role. It needs to Spring
     *
     * @param adminMemberRole String with admin member role
     */
    public void setAdminMemberRole(String adminMemberRole) {
        this.adminMemberRole = adminMemberRole;
    }

    /**
     * Returns admin list from file config
     *
     * @return List with admins usernames
     */
    public List getAdmins() {
        return admins;
    }

    /**
     * Setter to admins list. It needs for Spring
     *
     * @param admins admins list
     */
    public void setAdmins(List admins) {
        this.admins = admins;
    }

    /**
     * Checks is the user with username is initially admin
     *
     * @param userName user name
     * @return boolean result
     */
    public boolean isUserIsAdmin(Long userName) {
        return getAdmins().contains(userName);
    }

}
