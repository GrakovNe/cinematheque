package net.thumbtack.cinematheque.shedulers;


import net.thumbtack.cinematheque.services.RecommendationService;
import net.thumbtack.cinematheque.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Scheduled task for updating personal recommendations
 */
@Component
public class RebuildRecommendationsTask {
    private static final int RECOMMENDATION_UPDATE_PERIOD = 43_200_000;
    private Logger logger = LoggerFactory.getLogger(RebuildRecommendationsTask.class);
    @Autowired
    private RecommendationService recommendationService;
    @Autowired
    private UserService userService;

    /**
     * Scheduled method, which will update personal recommendations for all users every 12 hours
     */
    @Scheduled(fixedRate = RECOMMENDATION_UPDATE_PERIOD) // once per 12 hours
    public void rebuildRecommendations() {
        List< Long > activeUsers = userService.getActiveUsers();

        for (Long userId : activeUsers) {
            recommendationService.updateRecommendationsList(userId);
        }

        logger.info("Recommendations was updated");

    }
}
