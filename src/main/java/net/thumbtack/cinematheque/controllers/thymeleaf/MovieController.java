package net.thumbtack.cinematheque.controllers.thymeleaf;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.thumbtack.cinematheque.config.ApplicationConfigurationProvider;
import net.thumbtack.cinematheque.entity.FaceBookUser;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.exceptions.checkers.MovieChecker;
import net.thumbtack.cinematheque.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * UI Controller for providing movies functional
 */

@Controller
@RequestMapping("/movie")
public class MovieController {

    private static final String ORDER_BY_PREFIX = "orderby";
    private static final String ORDER_BY_TITLE = "title";
    private static final String ORDER_BY_YEAR = "year";
    private static final String ORDER_BY_RANK = "rank";
    private static final String ORDER_BY_NONE = "none";

    private static final String PAGE_NUMBER_PREFIX = "page";
    private static final String PAGE_SIZE_PREFIX = "pageSize";

    private static final String MODEL_PREFIX_USER = "user";
    private static final String MODEL_PREFIX_USER_ROLES = "userRoles";
    private static final String MODEL_PREFIX_MOVIE = "movie";
    private static final String MODEL_PREFIX_MOVIES = "movies";
    private static final String MODEL_PREFIX_PAGE_COUNT = "pageCount";
    private static final String MODEL_PREFIX_PAGE_CURRENT = "currentPage";
    private static final String MODEL_PREFIX_ACTIVE = "active";
    private static final String MODEL_PREFIX_STARS = "stars";
    private static final String MODEL_PREFIX_IN_WISH_LIST = "isMovieInWishList";
    private static final String MODEL_PREFIX_IN_BOOKMARK_LIST = "isMovieInBookmarkList";
    private static final String MODEL_PREFIX_MOVIE_SEARCHED = "isSearched";

    @Autowired
    private ApplicationConfigurationProvider provider;
    @Autowired
    private MoviesService moviesService;
    @Autowired
    private RankService rankService;
    @Autowired
    private StarService starService;
    @Autowired
    private UserService userService;
    @Autowired
    private RecommendationService recommendationService;
    private Logger logger = LoggerFactory.getLogger(MovieController.class);

    /**
     * Receives "/movies" queries and redirect em to movies list
     *
     * @return string with redirect for Spring
     */
    @RequestMapping()
    public String getRootMovies() {
        return "redirect:movie/all";
    }

    /**
     * Returns poster for required movie
     *
     * @param id movie identity
     * @return Image content
     */
    @RequestMapping("{id}/image")
    @ResponseBody
    public HttpEntity< byte[] > getMovieImage(@PathVariable Long id) {
        byte[] image = moviesService.getMovieById(id).getImage();
        MovieChecker.checkMovieImageFound(image);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(image.length);
        return new HttpEntity<>(image, headers);
    }

    /**
     * Adds new movie in DB
     *
     * @param authUser    Spring user auth
     * @param poster      multipart image
     * @param movieString Json string with movie data
     * @param model       Spring model
     * @return Spring redirection
     * @throws IOException if some resource isn't available
     */
    @RequestMapping(method = RequestMethod.POST)
    public String addMovie(@AuthenticationPrincipal OAuth2Authentication authUser, @RequestParam(value = "poster", required = false) MultipartFile poster, @RequestParam("movie") String movieString, Model model) throws IOException {

        /*We need convert string to entity by hands because of multipart*/
        Gson gson = new GsonBuilder().create();
        Movie movie = gson.fromJson(movieString, Movie.class);

        movie = moviesService.addMovie(movie);
        if (poster.getBytes().length > 0) {
            moviesService.addPosterToMovie(movie.getId(), poster.getBytes());
        }

        FaceBookUser user = new FaceBookUser(authUser);
        model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        model.addAttribute(MODEL_PREFIX_USER, user);
        return "movieList";
    }

    /**
     * Provides deleting movie from DB
     *
     * @param id       identity of deleting movie
     * @param model    Spring model
     * @param authUser Authorized user
     * @return Spring redirection
     */
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public String deleteMovie(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        moviesService.deleteMovie(id);

        FaceBookUser user = new FaceBookUser(authUser);
        model.addAttribute(MODEL_PREFIX_USER, user);
        model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        return "movieList";
    }

    /**
     * Returns page of top movies, which was liked much all
     *
     * @param model    Spring model
     * @param authUser authorized user
     * @return Spring redirection
     */
    @RequestMapping(value = "top")
    public String getTopMovies(@AuthenticationPrincipal OAuth2Authentication authUser, Model model) {
        model.addAttribute(MODEL_PREFIX_MOVIES, moviesService.getTopMovies());
        model.addAttribute(MODEL_PREFIX_ACTIVE, "top");
        if (authUser != null) {
            FaceBookUser user = new FaceBookUser(authUser);
            model.addAttribute(MODEL_PREFIX_USER, user);
            model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        }
        return "movieList";
    }

    /**
     * Returns page with personal recommended movies
     *
     * @param authUser Authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "recommend")
    public String getRecommendMovies(@AuthenticationPrincipal OAuth2Authentication authUser, Model model) {
        model.addAttribute(MODEL_PREFIX_ACTIVE, "recommend");
        if (authUser != null) {
            FaceBookUser user = new FaceBookUser(authUser);
            model.addAttribute(MODEL_PREFIX_MOVIES, recommendationService.getRecommendationsList(user.getId()));
            model.addAttribute(MODEL_PREFIX_USER, user);
            model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        }
        return "movieList";
    }

    /**
     * Returns page of newest movies, which was added recently
     *
     * @param authUser Authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "recent")
    public String getRecentMovies(@AuthenticationPrincipal OAuth2Authentication authUser, Model model) {
        model.addAttribute(MODEL_PREFIX_MOVIES, moviesService.getRecentMovies());
        model.addAttribute(MODEL_PREFIX_ACTIVE, "recent");
        if (authUser != null) {
            FaceBookUser user = new FaceBookUser(authUser);
            model.addAttribute(MODEL_PREFIX_USER, user);
            model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        }
        return "movieList";
    }

    /**
     * Provides getting all movies from DB
     *
     * @param model      Spring model
     * @param authUser   authorized user
     * @param ordering   String with ordering rule
     * @param pageNumber number of required page
     * @return Spring redirection
     */
    @RequestMapping("all")
    public String getAllMovies(@AuthenticationPrincipal OAuth2Authentication authUser, Model model, @RequestParam(value = ORDER_BY_PREFIX, defaultValue = ORDER_BY_TITLE) String ordering, @RequestParam(value = PAGE_NUMBER_PREFIX, defaultValue = "1") int pageNumber) {
        Page< Movie > resultMovies;
        switch (ordering) {
            case ORDER_BY_TITLE:
                resultMovies = moviesService.getMoviesOrderedByTitle(pageNumber);
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_TITLE);
                break;
            case ORDER_BY_YEAR:
                resultMovies = moviesService.getMoviesOrderedByYear(pageNumber);
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_YEAR);
                break;
            case ORDER_BY_RANK:
                resultMovies = moviesService.getMoviesOrderedByRank(pageNumber);
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_RANK);
                break;
            default:
                resultMovies = moviesService.getMoviesOrderedByTitle(pageNumber);
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_NONE);
        }

        fillModelWithMovieListData(resultMovies, authUser, pageNumber, model);
        model.addAttribute(MODEL_PREFIX_ACTIVE, "all");
        return "movieList";
    }

    /**
     * Provides getting wish list movies
     *
     * @param model      Spring model
     * @param ordering   string with ordering rule
     * @param authUser   authorized user
     * @param pageNumber number of required page
     * @return spring redirection
     */
    @RequestMapping("wishlist")
    public String getWishMovies(@AuthenticationPrincipal OAuth2Authentication authUser, Model model, @RequestParam(value = ORDER_BY_PREFIX, defaultValue = ORDER_BY_TITLE) String ordering, @RequestParam(value = PAGE_NUMBER_PREFIX, defaultValue = "1") int pageNumber) {
        FaceBookUser user = new FaceBookUser(authUser);

        Page< Movie > resultMovies;
        switch (ordering) {
            case ORDER_BY_TITLE:
                resultMovies = moviesService.getWishListMoviesOrderedByTitle(pageNumber, user.getId());
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_TITLE);
                break;
            case ORDER_BY_YEAR:
                resultMovies = moviesService.getWishListMoviesOrderedByYear(pageNumber, user.getId());
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_YEAR);
                break;
            case ORDER_BY_RANK:
                resultMovies = moviesService.getWishListMoviesOrderedByRank(pageNumber, user.getId());
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_RANK);
                break;
            default:
                resultMovies = moviesService.getWishListMoviesOrderedByTitle(1, user.getId());
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_NONE);
        }

        fillModelWithMovieListData(resultMovies, authUser, pageNumber, model);
        model.addAttribute(MODEL_PREFIX_ACTIVE, "wishlist");
        return "movieList";
    }

    /**
     * Provides getting bookmark list movies
     *
     * @param model      Spring model
     * @param authUser   authorized user
     * @param ordering   string with ordering rule
     * @param pageNumber number of required page
     * @return spring redirection
     */

    @RequestMapping("bookmarklist")
    public String getBookmarkMovies(@AuthenticationPrincipal OAuth2Authentication authUser, Model model, @RequestParam(value = ORDER_BY_PREFIX, defaultValue = ORDER_BY_TITLE) String ordering, @RequestParam(value = PAGE_NUMBER_PREFIX, defaultValue = "1") int pageNumber) {
        FaceBookUser user = new FaceBookUser(authUser);

        Page< Movie > resultMovies;
        switch (ordering) {
            case ORDER_BY_TITLE:
                resultMovies = moviesService.getBookmarkListMoviesOrderedByTitle(pageNumber, user.getId());
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_TITLE);
                break;
            case ORDER_BY_YEAR:
                resultMovies = moviesService.getBookmarkListMoviesOrderedByYear(pageNumber, user.getId());
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_YEAR);
                break;
            case ORDER_BY_RANK:
                resultMovies = moviesService.getBookmarkListMoviesOrderedByRank(pageNumber, user.getId());
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_RANK);
                break;
            default:
                resultMovies = moviesService.getBookmarkListMoviesOrderedByTitle(1, user.getId());
                model.addAttribute(ORDER_BY_PREFIX, ORDER_BY_NONE);
        }

        fillModelWithMovieListData(resultMovies, authUser, pageNumber, model);
        model.addAttribute(MODEL_PREFIX_ACTIVE, "bookmarklist");
        return "movieList";
    }

    /**
     * Provides getting movie by identity
     *
     * @param id       identity of required movie
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping("{id}")
    public String getMovieById(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        fillModelWithMovieData(id, authUser, model);
        return "movie";
    }

    /**
     * Returns form for attaching star to movie
     *
     * @param id       identity of current movie
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection to form
     */
    @RequestMapping("{id}/attachStarForm")
    public String attachStarForm(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        fillModelWithMovieData(id, authUser, model);
        return "attachStarForm";
    }

    /**
     * Provides adding movie to wish list
     *
     * @param id       identity of movie
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "{id}/wishlist", method = RequestMethod.POST)
    public String addMovieToWishList(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        FaceBookUser user = new FaceBookUser(authUser);
        moviesService.addMovieToWishList(id, user.getId());
        fillModelWithMovieData(id, authUser, model);
        return "movie";
    }

    /**
     * Provides adding movie to bookmark list
     *
     * @param id       identity of adding movie
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "{id}/bookmarklist", method = RequestMethod.POST)
    public String addMovieToBookmarkList(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        FaceBookUser user = new FaceBookUser(authUser);
        moviesService.addMovieToBookmarkList(id, user.getId());
        fillModelWithMovieData(id, authUser, model);
        return "movie";
    }

    /**
     * Provides removing movie from wish list
     *
     * @param id       identity of movie
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "{id}/wishlist", method = RequestMethod.DELETE)
    public String removeMovieFromWishList(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        FaceBookUser user = new FaceBookUser(authUser);
        moviesService.removeMovieFromWishList(id, user.getId());
        fillModelWithMovieData(id, authUser, model);
        return "movie";
    }

    /**
     * Provides removing movie from bookmark list
     *
     * @param id       identity of movie
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "{id}/bookmarklist", method = RequestMethod.DELETE)
    public String removeMovieFromBookmarkList(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        FaceBookUser user = new FaceBookUser(authUser);
        moviesService.removeMovieFromBookmarkList(id, user.getId());
        fillModelWithMovieData(id, authUser, model);
        return "movie";
    }

    /**
     * Redirects to form for adding movie
     *
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping("addMovieForm")
    public String showAddMovieForm(@AuthenticationPrincipal OAuth2Authentication authUser, Model model) {
        FaceBookUser user = new FaceBookUser(authUser);
        model.addAttribute(MODEL_PREFIX_USER, user);
        model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        return "addMovieForm";
    }

    /**
     * Returns form for OMDB movie import
     *
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping("addOMDBMovieForm")
    public String showAddOMDBMovieForm(@AuthenticationPrincipal OAuth2Authentication authUser, Model model) {
        FaceBookUser user = new FaceBookUser(authUser);
        model.addAttribute(MODEL_PREFIX_USER, user);
        model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        return "addOMDBMovieForm";
    }

    /**
     * Adds movie to cinematheque from OMDB library
     *
     * @param authUser authorized user
     * @param title    movie title
     * @param model    Sprint model
     * @return Spring redirection
     */
    @RequestMapping(value = "import", method = RequestMethod.POST)
    public String addOMDBMovie(@AuthenticationPrincipal OAuth2Authentication authUser, @RequestParam String title, Model model) {
        Movie createdMovie = moviesService.importMovieFromOMBD(title);
        fillModelWithMovieData(createdMovie.getId(), authUser, model);
        return "movieList";
    }

    /**
     * Redirects to updating movie form
     *
     * @param id       identity of updating movie
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping("{id}/editMovie")
    public String editMovieForm(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        moviesService.getMovieById(id);
        fillModelWithMovieData(id, authUser, model);
        return "editMovieForm";
    }

    /**
     * Updates movie in DB
     *
     * @param authUser    Spring auth user data
     * @param id          identity of movie
     * @param poster      new poster image
     * @param movieString new movie data
     * @param model       Spring model
     * @return Spring redirection to updated movie
     * @throws IOException if poster upload broken
     */
    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String editMovie(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, @RequestParam(value = "poster", required = false) MultipartFile poster, @RequestParam("movie") String movieString, Model model) throws IOException {
        Gson gson = new GsonBuilder().create();
        Movie movie = gson.fromJson(movieString, Movie.class);

        Movie reachedMovie = moviesService.updateMovie(id, movie);

        if (poster.getBytes().length > 0) {
            moviesService.addPosterToMovie(reachedMovie.getId(), poster.getBytes());
        }

        fillModelWithMovieData(id, authUser, model);
        return "movie";
    }

    /**
     * Provides ranking movie
     *
     * @param id       identity of movie
     * @param authUser authorized user
     * @param value    rank value, positive integer between 1 and 5
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "{id}/rank/", method = RequestMethod.PUT)
    public String rankMovie(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, @RequestParam Integer value, Model model) {
        rankService.addOrUpdateRank(id, value);
        fillModelWithMovieData(id, authUser, model);
        return "movie";
    }

    /**
     * Provides attaching star to movie
     *
     * @param authUser authorized user
     * @param movieId  identity of movie
     * @param starId   identity of star
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "{movieId}/star/{starId}", method = RequestMethod.POST)
    public String addStarToMovie(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long movieId, @PathVariable Long starId, Model model) {
        moviesService.addStar(movieId, starId);
        fillModelWithMovieData(movieId, authUser, model);
        return "movie";
    }

    /**
     * Provides detaching star to movie
     *
     * @param authUser authorized user
     * @param movieId  identity of movie
     * @param starId   identity of star
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "{movieId}/star/{starId}", method = RequestMethod.DELETE)
    public String deleteStarFromMovie(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long movieId, @PathVariable Long starId, Model model) {
        moviesService.removeStar(movieId, starId);
        fillModelWithMovieData(movieId, authUser, model);
        return "movie";
    }


    /**
     * Provides searching movie by them title
     *
     * @param authUser     authorized user
     * @param searchPhrase keyword for search
     * @param model        Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String searchMovie(@AuthenticationPrincipal OAuth2Authentication authUser, @RequestParam String searchPhrase, Model model) {
        List< Movie > movieList = moviesService.cascadeSearchMovie(searchPhrase);

        if (movieList.isEmpty() && moviesService.isMovieExistsInOmdb(searchPhrase)) {
            movieList.add(moviesService.importMovieFromOMBD(searchPhrase));
        }

        if (movieList.size() == 1) {
            return getMovieById(authUser, movieList.get(0).getId(), model);
        }

        FaceBookUser user = new FaceBookUser(authUser);
        model.addAttribute(MODEL_PREFIX_MOVIE, movieList);
        model.addAttribute(MODEL_PREFIX_ACTIVE, "all");
        model.addAttribute(MODEL_PREFIX_MOVIE_SEARCHED, true);
        model.addAttribute(MODEL_PREFIX_USER, user);
        model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));

        return "movieList";
    }

    private void fillModelWithMovieData(Long movieId, OAuth2Authentication authUser, Model model) {
        model.addAttribute(MODEL_PREFIX_MOVIE, moviesService.getMovieById(movieId));
        model.addAttribute(MODEL_PREFIX_STARS, starService.getStars());
        if (authUser != null) {
            FaceBookUser user = new FaceBookUser(authUser);
            model.addAttribute(MODEL_PREFIX_IN_WISH_LIST, moviesService.isMovieInWishList(movieId, user.getId()));
            model.addAttribute(MODEL_PREFIX_IN_BOOKMARK_LIST, moviesService.isMovieInBookmarkList(movieId, user.getId()));
            model.addAttribute(MODEL_PREFIX_USER, user);
            model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        }

    }

    private void fillModelWithMovieListData(Page< Movie > resultMovies, OAuth2Authentication auth2Authentication, int pageNumber, Model model) {
        model.addAttribute(MODEL_PREFIX_MOVIES, resultMovies);
        model.addAttribute(MODEL_PREFIX_PAGE_COUNT, resultMovies.getTotalPages());
        model.addAttribute(MODEL_PREFIX_PAGE_CURRENT, pageNumber);
        model.addAttribute(PAGE_SIZE_PREFIX, provider.getPageSize());
        if (auth2Authentication != null) {
            FaceBookUser user = new FaceBookUser(auth2Authentication);
            model.addAttribute(MODEL_PREFIX_USER, user);
            model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        }
    }
}