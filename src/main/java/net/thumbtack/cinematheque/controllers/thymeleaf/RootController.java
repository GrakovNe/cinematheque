package net.thumbtack.cinematheque.controllers.thymeleaf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * UI Root controller. Contains handling "/" request only and no functions.
 */

@Controller
public class RootController {

    private Logger logger = LoggerFactory.getLogger(RootController.class);

    /**
     * Root controller, redirecting to cinematheque root instead of app root
     *
     * @return Spring redirection
     */
    @RequestMapping("/")
    String index() {
        return "redirect:movie/all";
    }

    @RequestMapping("/out")
    String logout(HttpServletRequest request) {
        request.getSession().invalidate();
        SecurityContextHolder.getContext().setAuthentication(null);
        return "redirect:movie/all";
    }
}
