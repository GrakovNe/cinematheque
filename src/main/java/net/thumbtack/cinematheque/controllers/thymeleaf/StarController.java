package net.thumbtack.cinematheque.controllers.thymeleaf;

import net.thumbtack.cinematheque.config.ApplicationConfigurationProvider;
import net.thumbtack.cinematheque.entity.FaceBookUser;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.services.StarService;
import net.thumbtack.cinematheque.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * UI Controller for providing star functional
 */

@Controller
@RequestMapping("/star")
public class StarController {

    private static final String PAGE_NUMBER_PREFIX = "page";
    private static final String PAGE_SIZE_PREFIX = "pageSize";

    private static final String MODEL_PREFIX_USER = "user";
    private static final String MODEL_PREFIX_USER_ROLES = "userRoles";

    private static final String MODEL_PREFIX_STARS = "stars";
    private static final String MODEL_PREFIX_STAR = "star";
    private static final String MODEL_PREFIX_PAGE_CURRENT = "currentPage";
    private static final String MODEL_PREFIX_PAGE_COUNT = "pageCount";

    @Autowired
    private ApplicationConfigurationProvider provider;
    @Autowired
    private StarService starService;
    @Autowired
    private UserService userService;
    private Logger logger = LoggerFactory.getLogger(StarController.class);

    /**
     * Provides getting all stars in DB
     *
     * @param model      Spring model
     * @param authUser   authorized user
     * @param pageNumber number of required page
     * @return Movies list in model
     */
    @RequestMapping()
    public String starsList(@AuthenticationPrincipal OAuth2Authentication authUser, Model model, @RequestParam(value = PAGE_NUMBER_PREFIX, defaultValue = "1") int pageNumber) {
        Page< Star > stars = starService.getStars(pageNumber);
        model.addAttribute(MODEL_PREFIX_STARS, stars);
        model.addAttribute(MODEL_PREFIX_PAGE_CURRENT, pageNumber);
        model.addAttribute(MODEL_PREFIX_PAGE_COUNT, stars.getTotalPages());
        model.addAttribute(PAGE_SIZE_PREFIX, provider.getPageSize());
        fillWithUserInformation(authUser, model);
        return "starList";
    }

    /**
     * Redirects to new star adding form
     *
     * @param authUser authorized user
     * @param model    Spring model
     * @return Spring redirection
     */
    @RequestMapping("addStarForm")
    public String addStarForm(@AuthenticationPrincipal OAuth2Authentication authUser, Model model) {
        fillWithUserInformation(authUser, model);
        return "addStarForm";
    }

    /**
     * Provides adding new star in DB
     *
     * @param star     Received from client star
     * @param model    Spring model
     * @param authUser authorized user
     * @return Spring redirection
     */
    @RequestMapping(method = RequestMethod.POST)
    public String addStar(@AuthenticationPrincipal OAuth2Authentication authUser, @RequestBody Star star, Model model) {
        starService.addStar(star);
        fillWithUserInformation(authUser, model);
        return "starList";
    }

    /**
     * Provides removing star from DB
     *
     * @param authUser authorized user
     * @param model    Spring model
     * @param id       identity of star
     * @return Spring redirection
     */
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public String removeStar(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        starService.deleteStar(id);
        fillWithUserInformation(authUser, model);
        return "starList";
    }

    /**
     * Redirects to form for edit movie
     *
     * @param id       identity of updating movie
     * @param model    Spring model
     * @param authUser authorized user
     * @return Spring redirection
     */
    @RequestMapping("{id}/editStar")
    public String editMovieForm(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, Model model) {
        Star star = starService.getStarById(id);
        model.addAttribute(MODEL_PREFIX_STAR, star);
        fillWithUserInformation(authUser, model);
        return "editStarForm";
    }

    /**
     * Provides updating star data
     *
     * @param id       identity of updating star
     * @param authUser authorized user
     * @param model    Spring model
     * @param star     actual star data
     * @return Spring redirection
     */
    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String editStar(@AuthenticationPrincipal OAuth2Authentication authUser, @PathVariable Long id, @RequestBody Star star, Model model) {
        starService.getStarById(id);

        if (StringUtils.isEmpty(star.getMiddleName())) {
            star.setMiddleName(" ");
        }

        starService.updateStar(id, star);
        fillWithUserInformation(authUser, model);
        return "starList";
    }

    /**
     * Searches stars in DB by their last names
     *
     * @param authUser     authorized user
     * @param searchPhrase key word for search
     * @param model        Spring model
     * @return Spring redirection
     */
    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String searchStar(@AuthenticationPrincipal OAuth2Authentication authUser, @RequestParam String searchPhrase, Model model) {
        model.addAttribute(MODEL_PREFIX_STARS, starService.cascadeSearchStar(searchPhrase));
        fillWithUserInformation(authUser, model);
        return "starList";
    }

    private void fillWithUserInformation(OAuth2Authentication authUser, Model model) {
        if (authUser != null) {
            FaceBookUser user = new FaceBookUser(authUser);
            model.addAttribute(MODEL_PREFIX_USER, user);
            model.addAttribute(MODEL_PREFIX_USER_ROLES, userService.getOauthUserRoles(user.getId()));
        }
    }
}
