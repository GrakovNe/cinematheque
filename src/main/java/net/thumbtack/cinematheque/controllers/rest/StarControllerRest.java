package net.thumbtack.cinematheque.controllers.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.exceptions.checkers.AuthChecker;
import net.thumbtack.cinematheque.services.StarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for providing Stars functional
 */

@RestController
@RequestMapping("/api/v1/star")
@Api(value = "/api/v1/star", description = "Stars operations")
public class StarControllerRest {
    private final Logger logger = LoggerFactory.getLogger(MovieControllerRest.class);
    @Autowired
    private StarService starService;

    /**
     * Returns list with data of all stars in DB
     *
     * @return Stars list
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "get stars", nickname = "getStarsList")
    public List< Star > getStarsList() {
        return starService.getStars();
    }

    /**
     * Returns data for one required Star entity by id
     *
     * @param id identity of required star
     * @return Rank entity
     */
    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "get star", nickname = "getStarById")
    public Star getStarById(@PathVariable Long id) {
        return starService.getStarById(id);
    }

    /**
     * Returns data for required Stars entity by last name
     *
     * @param lastName last name of required entity
     * @return List with rank entities
     */
    @RequestMapping(value = "lastname/{lastName}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "search stars by them last names", nickname = "getStarByLastName")
    public List< Star > getStarByLastName(@PathVariable String lastName) {
        return starService.getStarsByLastName(lastName);
    }

    /**
     * Returns data for required Stars entity by first name
     *
     * @param firstName first name of required entity
     * @return List with rank entities
     */
    @RequestMapping(value = "firstname/{firstName}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "search stars by them first names", nickname = "getStarByFirstName")
    public List< Star > getStarByFirstName(@PathVariable String firstName) {
        return starService.getByFirstName(firstName);
    }

    /**
     * Adds new star id DB
     *
     * @param star new star entity
     * @param user authorized user
     * @return created star
     */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "add new star", nickname = "addStar")
    public Star addStar(@AuthenticationPrincipal OAuth2Authentication user, @RequestBody Star star) {
        AuthChecker.checkAuth(user);
        return starService.addStar(star);
    }

    /**
     * Deletes star from DB
     *
     * @param user authorized user
     * @param id   identity of deleting star
     * @return Operation result
     */
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "delete star", nickname = "deleteStar")
    public ResponseEntity deleteStar(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id) {
        AuthChecker.checkAuth(user);
        starService.deleteStar(id);
        return ResponseEntity.ok().build();
    }

    /**
     * Updated star in DB
     *
     * @param user authorized user
     * @param id   identity of updating star
     * @param star actual star identity
     * @return updated star
     */
    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
    @ApiOperation(value = "update star", nickname = "updateStar")
    public Star updateStar(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id, @RequestBody Star star) {
        AuthChecker.checkAuth(user);
        return starService.updateStar(id, star);
    }

    /**
     * Searches stars in DB with cascade search
     *
     * @param searchPhrase search key
     * @return List of found stars
     */
    @RequestMapping(value = "search", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "search star", nickname = "searchStar")
    public List< Star > searchStar(@RequestParam String searchPhrase) {
        return starService.cascadeSearchStar(searchPhrase);
    }

}
