package net.thumbtack.cinematheque.controllers.rest;

import io.swagger.annotations.ApiOperation;
import net.thumbtack.cinematheque.entity.FaceBookUser;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.exceptions.checkers.AuthChecker;
import net.thumbtack.cinematheque.services.MoviesService;
import net.thumbtack.cinematheque.services.RankService;
import net.thumbtack.cinematheque.services.RecommendationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * REST controller for providing movies functional
 */

@RestController
@RequestMapping("/api/v1/movie")
public class MovieControllerRest {
    private final Logger logger = LoggerFactory.getLogger(MovieControllerRest.class);

    @Autowired
    private MoviesService moviesService;
    @Autowired
    private RankService rankService;
    @Autowired
    private RecommendationService recommendationService;

    /**
     * Adds new poster to movie
     *
     * @param id     movie identity
     * @param poster image binary data
     * @return updated movie
     * @throws IOException if uploading is broken
     */
    @RequestMapping(value = "{id}/image", method = RequestMethod.POST)
    public Movie addImageToMovie(@PathVariable Long id, @RequestParam("poster") MultipartFile poster) throws IOException {
        return moviesService.addPosterToMovie(id, poster.getBytes());
    }

    /**
     * Returns movies list with all available movies from DB
     *
     * @return List with all movies in DB
     */

    @ApiOperation(value = "get movies list", nickname = "getMoviesList", produces = "application/json")
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List< Movie > getMoviesList() {
        return moviesService.getMovies();
    }

    /**
     * Returns top (mostly liked) movie from DB
     *
     * @return List with movies
     */
    @ApiOperation(value = "get top movies list", nickname = "getTopMoviesList", produces = "application/json")
    @RequestMapping(value = "top", method = RequestMethod.GET, produces = "application/json")
    public List< Movie > getTopMovies() {
        return moviesService.getTopMovies();
    }

    /**
     * Returns recent movies from DB
     *
     * @return List with movies
     */
    @ApiOperation(value = "get recent movies list", nickname = "getRecentMoviesList", produces = "application/json")
    @RequestMapping(value = "recent", method = RequestMethod.GET, produces = "application/json")
    public List< Movie > getRecentMovies() {
        return moviesService.getRecentMovies();
    }

    /**
     * returns recommended movie for authorized user
     *
     * @param user authorized user
     * @return List with movies
     */
    @ApiOperation(value = "get recommended movies list", nickname = "getRecommendedMoviesList", produces = "application/json")
    @RequestMapping(value = "recommend", method = RequestMethod.GET, produces = "application/json")
    public List< Movie > getRecommendedMovies(@AuthenticationPrincipal OAuth2Authentication user) {
        AuthChecker.checkAuth(user);
        return recommendationService.getRecommendationsList(new FaceBookUser(user).getId());
    }

    /**
     * Returns movie by id from DB
     *
     * @param id identity of required movie
     * @return one Movie object with rank (if there is) and stars list
     */
    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "get movie", nickname = "getMovie")
    public Movie getMovie(@PathVariable Long id) {
        return moviesService.getMovieById(id);
    }

    /**
     * deletes movie from DB
     *
     * @param user authorized user
     * @param id   identity of deleting movie
     * @return Response with ok or exception if movie not found in DB
     */
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "delete movie", nickname = "deleteMovie")
    public ResponseEntity deleteMovie(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id) {
        AuthChecker.checkAuth(user);
        moviesService.deleteMovie(id);
        return ResponseEntity.ok().build();
    }

    /**
     * Adds star to star list of selected movie. Star should be in DB before and shouldn't contains in stars list already
     *
     * @param movieId identity of selected movie
     * @param user    authorized user
     * @param starId  identity of selected star
     * @return updated movie entity
     */
    @RequestMapping(value = "{movieId}/star/{starId}", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "attach star to movie", nickname = "addStar")
    public Movie addStar(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long movieId, @PathVariable Long starId) {
        AuthChecker.checkAuth(user);
        return moviesService.addStar(movieId, starId);
    }

    /**
     * removes star from movie star list. Star should be in DB before and contain in stars list already
     *
     * @param movieId identity of selected movie
     * @param user    authorized user
     * @param starId  identity of selected star
     * @return updated movie entity
     */
    @RequestMapping(value = "{movieId}/star/{starId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "detach star from movie", nickname = "removeStar")
    public Movie removeStar(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long movieId, @PathVariable Long starId) {
        AuthChecker.checkAuth(user);
        return moviesService.removeStar(movieId, starId);
    }

    /**
     * Ranks movie with value
     *
     * @param id    identity of selected movie
     * @param user  authorized user
     * @param value positive integer between 1 and 5
     * @return Created rank
     */
    @RequestMapping(value = "{id}/rank", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "rank movie", nickname = "removeStar")
    public Rank rankMovie(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id, @RequestParam Integer value) {
        AuthChecker.checkAuth(user);
        return rankService.addRank(id, value);
    }

    /**
     * Adds movie to DB
     *
     * @param movie new movie for insert
     * @param user  authorized user
     * @return updated movie entity with ID and empty rank, star list
     */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "add movie", nickname = "addMovie")
    public Movie addMovie(@AuthenticationPrincipal OAuth2Authentication user, @RequestBody Movie movie) {
        AuthChecker.checkAuth(user);
        return moviesService.addMovie(movie);
    }

    /**
     * Adds movie to wish list
     *
     * @param id   identity of movie
     * @param user authorized user
     * @return updated movie
     */
    @RequestMapping(value = "{id}/wishlist", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "add movie to wish list", nickname = "addMovieToWishList")
    public Movie addMovieToWishList(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id) {
        AuthChecker.checkAuth(user);
        return moviesService.addMovieToWishList(id, new FaceBookUser(user).getId());
    }

    /**
     * Removes movie from wish list
     *
     * @param id   identity of movie
     * @param user authorized user
     * @return updated movie
     */
    @RequestMapping(value = "{id}/wishlist", method = RequestMethod.DELETE, produces = "application/json")
    @ApiOperation(value = "remove movie from wish list", nickname = "removeMovieFromWishList")
    public Movie removeMovieFromWishList(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id) {
        AuthChecker.checkAuth(user);
        return moviesService.removeMovieFromWishList(id, new FaceBookUser(user).getId());
    }

    /**
     * Returns list of wish list movies
     *
     * @param user authorized user
     * @return list of wish list movies
     */
    @RequestMapping(value = "wishlist", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "get movies in wish list", nickname = "getWishListMovies")
    public List< Movie > getWishListMovies(@AuthenticationPrincipal OAuth2Authentication user) {
        AuthChecker.checkAuth(user);
        return moviesService.getWishListMovies(new FaceBookUser(user).getId());
    }

    /**
     * Adds movie to bookmark list
     *
     * @param id   identity of movie
     * @param user authorized user
     * @return updated movie
     */
    @RequestMapping(value = "{id}/bookmarklist", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "add movie to bookmark list", nickname = "addMovieToBookmarkList")
    public Movie addMovieToBookmarkList(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id) {
        AuthChecker.checkAuth(user);
        return moviesService.addMovieToBookmarkList(id, new FaceBookUser(user).getId());
    }

    /**
     * Removes movie from bookmarklist
     *
     * @param id   identity of movie
     * @param user authorized user
     * @return Updated movie
     */
    @RequestMapping(value = "{id}/bookmarklist", method = RequestMethod.DELETE, produces = "application/json")
    @ApiOperation(value = "removie movie from bookmark list", nickname = "removeMovieFromBookmarkList")
    public Movie removeMovieFromBookmarkList(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id) {
        AuthChecker.checkAuth(user);
        return moviesService.removeMovieFromBookmarkList(id, new FaceBookUser(user).getId());
    }

    /**
     * Returns list on bookmark list movies
     *
     * @param user authorized user
     * @return List of bookmark movies
     */
    @RequestMapping(value = "bookmarklist", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "get movies in bookmark list", nickname = "getBookmarkListMovies")
    public List< Movie > getBookmarkListMovies(@AuthenticationPrincipal OAuth2Authentication user) {
        AuthChecker.checkAuth(user);
        return moviesService.getBookmarkListMovies(new FaceBookUser(user).getId());
    }

    /**
     * Updates movie in BD
     *
     * @param id    identity of updated movie
     * @param movie actual movie entity
     * @param user  authorized user
     * @return updated movie
     */
    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
    @ApiOperation(value = "update movie data", nickname = "updateMovie")
    public Movie updateMovie(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id, @RequestBody Movie movie) {
        AuthChecker.checkAuth(user);
        return moviesService.updateMovie(id, movie);
    }

    /**
     * Searches movies in DB with cascade search
     *
     * @param searchPhrase search key phrase
     * @return List with found movies
     */
    @RequestMapping(value = "search", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "search movie", nickname = "searchMovie")
    public List< Movie > searchMovie(@RequestParam String searchPhrase) {
        return moviesService.cascadeSearchMovie(searchPhrase);
    }


}
