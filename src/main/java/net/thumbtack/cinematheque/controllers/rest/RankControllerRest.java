package net.thumbtack.cinematheque.controllers.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.exceptions.checkers.AuthChecker;
import net.thumbtack.cinematheque.services.RankService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for providing ranks functional
 */
@RestController
@RequestMapping("/api/v1/rank")
@Api(value = "/api/v1/rank", description = "Ranks operations")
public class RankControllerRest {
    private final Logger logger = LoggerFactory.getLogger(MovieControllerRest.class);

    @Autowired
    private RankService rankService;

    /**
     * Returns all ranks for ranked movies in DB
     *
     * @return ranks list
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "get ranks for movies", nickname = "getRanksList")
    public List< Rank > getRanksList() {
        return rankService.getRanks();
    }

    /**
     * Returns one required rank by it ID
     *
     * @param id identity of required rank
     * @return rank entity
     */
    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "get ranks by identity", nickname = "getRankById")
    public Rank getRankById(@PathVariable Long id) {
        return rankService.getRankById(id);
    }

    /**
     * Updated rank entity to DB
     *
     * @param id   identity of updating rank
     * @param rank rank entity
     * @param user authorized user
     * @return updated rank entity
     */
    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
    @ApiOperation(value = "update rank with new data", nickname = "updateRank")
    public Rank updateRank(@AuthenticationPrincipal OAuth2Authentication user, @PathVariable Long id, @RequestBody Rank rank) {
        AuthChecker.checkAuth(user);
        return rankService.updateRank(id, rank);
    }

}
