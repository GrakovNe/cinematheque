package net.thumbtack.cinematheque.repositories;

import net.thumbtack.cinematheque.entity.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring JPA Movies repository
 */

@Transactional
public interface MoviesRepository extends JpaRepository< Movie, Long > {
    List< Movie > findByTitleStartsWithIgnoreCase(String title);

    List< Movie > findByYear(int year);

    Page< Movie > findAllByOrderByTitle(Pageable pageable);

    Page< Movie > findAllByOrderByYearDesc(Pageable pageable);

    @Query("from Movie order by rank.rank desc")
    List< Movie > findAllByOrderByRank();

    @Query("from Movie m order by rank.rank desc")
    Page< Movie > findAllByOrderByRank(Pageable pageable);

    List< Movie > findByTitleAndYear(String title, int year);

    @Query("from Movie where id in (select movieId from WishListMovie w where w.userId = ?1)")
    List< Movie > findAllInWishList(Long userId);

    @Query("from Movie m where id in (select movieId from WishListMovie w where w.userId = ?1) order by title")
    Page< Movie > findAllInWishListOrderByTitle(Pageable pageable, Long myUserId);

    @Query("from Movie m where id in (select movieId from WishListMovie w where w.userId = ?1) order by rank.rank desc")
    Page< Movie > findAllInWishListOrderByRank(Pageable pageable, Long myUserId);

    @Query("from Movie where id in (select movieId from WishListMovie w where w.userId = ?1) order by year desc")
    List< Movie > findAllInWishListOrderByYear(Long myUserId);

    @Query("from Movie m where id in (select movieId from WishListMovie w where w.userId = ?1) order by year desc")
    Page< Movie > findAllInWishListOrderByYear(Pageable pageable, Long myUserId);

    @Query("from Movie where id in (select movieId from BookmarkListMovie w where w.userId = ?1) order by title")
    List< Movie > findAllInBookmarkListOrderByTitle(Long myUserId);

    @Query("from Movie m where id in (select movieId from BookmarkListMovie w where w.userId = ?1) order by title")
    Page< Movie > findAllInBookmarkListOrderByTitle(Pageable pageable, Long myUserId);

    @Query("from Movie m where id in (select movieId from BookmarkListMovie w where w.userId = ?1) order by year desc")
    Page< Movie > findAllInBookmarkListOrderByYear(Pageable pageable, Long myUserId);

    @Query("from Movie where id in (select movieId from BookmarkListMovie w where w.userId = ?1) order by rank.rank desc")
    List< Movie > findAllInBookmarkListOrderByRank(Long myUserId);

    @Query("from Movie m where id in (select movieId from BookmarkListMovie w where w.userId = ?1) order by rank.rank desc")
    Page< Movie > findAllInBookmarkListOrderByRank(Pageable pageable, Long myUserId);

    @Query("from Movie m where id in (select movieId from WishListMovie group by movieId order by count (movieId))")
    List< Movie > findTopMovies();

    @Query("from Movie order by id desc")
    List< Movie > findRecentMovies();

    @Query("from Movie m where id in (select movieId from RecommendedListMovie w where w.userId = ?1)")
    List< Movie > findAllRecommendedMovie(Long userId);

}
