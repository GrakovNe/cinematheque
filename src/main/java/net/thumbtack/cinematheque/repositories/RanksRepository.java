package net.thumbtack.cinematheque.repositories;

import net.thumbtack.cinematheque.entity.Rank;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring JPA Ranks repository
 */

public interface RanksRepository extends JpaRepository< Rank, Long > {

}
