package net.thumbtack.cinematheque.repositories;

import net.thumbtack.cinematheque.entity.listmovies.RecommendedListMovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring JPA recommended movies repository
 */
public interface RecommendedMoviesRepository extends JpaRepository< RecommendedListMovie, Long > {
    @Modifying
    @Transactional
    @Query("delete from RecommendedListMovie r where r.userId = ?1")
    void deleteByUserId(Long userId);
}
