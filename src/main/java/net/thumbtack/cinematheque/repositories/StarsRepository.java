package net.thumbtack.cinematheque.repositories;

import net.thumbtack.cinematheque.entity.Star;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring JPA stars repository
 */

public interface StarsRepository extends JpaRepository< Star, Long > {
    List< Star > findByFirstNameIgnoreCase(String firstName);

    List< Star > findByLastNameStartsWithIgnoreCase(String lastName);

    List< Star > findByMiddleNameIgnoreCase(String middleName);

    List< Star > findByLastNameIgnoreCase(String lastName);

    Page< Star > findAllByOrderByFirstName(Pageable pageable);

    List< Star > findByFirstNameAndLastNameIgnoreCase(String firstName, String lastName);

    Star findByFirstNameAndMiddleNameAndLastNameIgnoreCase(String firstName, String middleName, String lastName);
}
