package net.thumbtack.cinematheque.repositories;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.listmovies.WishListMovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Spring JPA wish list repository
 */

public interface WishListMoviesRepository extends JpaRepository< WishListMovie, Long > {
    WishListMovie findByMovieIdAndUserId(Long movieId, Long userId);

    @Query("select distinct userId from WishListMovie")
    List< Long > findActiveUsers();

    @Query("select count(id) from WishListMovie where userId = ?1 and movieId in (select movieId from WishListMovie where userId = ?2)")
    int getUsersIntersectionCount(Long firstUser, Long secondUser);

    @Query("from Movie m where id in (select movieId from WishListMovie where userId = ?1 and movieId not in (select movieId from WishListMovie where userId = ?2))")
    List< Movie > getWishListDifference(Long firstUser, Long secondUser);

    @Query("select count(id) from WishListMovie where userId = ?1")
    int getUserWishListSize(Long user);

}
