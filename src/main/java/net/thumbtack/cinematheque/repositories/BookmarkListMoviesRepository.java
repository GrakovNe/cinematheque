package net.thumbtack.cinematheque.repositories;

import net.thumbtack.cinematheque.entity.listmovies.BookmarkListMovie;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring JPA bookmark movies repository
 */


public interface BookmarkListMoviesRepository extends JpaRepository< BookmarkListMovie, Long > {
    BookmarkListMovie findByMovieIdAndUserId(Long movieId, Long userId);
}
