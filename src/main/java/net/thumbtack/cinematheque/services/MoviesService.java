package net.thumbtack.cinematheque.services;

import com.google.common.collect.Lists;
import net.thumbtack.cinematheque.config.ApplicationConfigurationProvider;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.entity.listmovies.BookmarkListMovie;
import net.thumbtack.cinematheque.entity.listmovies.WishListMovie;
import net.thumbtack.cinematheque.exceptions.checkers.MovieChecker;
import net.thumbtack.cinematheque.exceptions.checkers.StarChecker;
import net.thumbtack.cinematheque.exceptions.moviesexceptions.MovieAlreadyExistException;
import net.thumbtack.cinematheque.exceptions.moviesexceptions.OMDBImportException;
import net.thumbtack.cinematheque.omdb.api.ApiFactory;
import net.thumbtack.cinematheque.omdb.converters.OmdbMovieConverter;
import net.thumbtack.cinematheque.omdb.entity.OMDBMovie;
import net.thumbtack.cinematheque.repositories.*;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service layer for movies functional
 */

@Service
public class MoviesService {
    private final Logger logger = LoggerFactory.getLogger(MoviesService.class);
    @Autowired
    private ApplicationConfigurationProvider provider;
    @Autowired
    private MoviesRepository moviesRepository;
    @Autowired
    private StarsRepository starsRepository;
    @Autowired
    private RanksRepository ranksRepository;
    @Autowired
    private WishListMoviesRepository wishListMoviesRepository;
    @Autowired
    private BookmarkListMoviesRepository bookmarkListMoviesRepository;

    /**
     * Adds poster to movie
     *
     * @param id    movie identity
     * @param image poster raw data
     * @return updated movie
     */
    public Movie addPosterToMovie(Long id, byte[] image) {
        Movie movie = getMovieById(id);

        MovieChecker.checkMovieImageFound(image);

        try {
            movie.setImage(resizeImage(image));
        } catch (IOException ex) {
            logger.warn("Can't resize image to movie with id: " + id + ". Make it manually!");
            // We have to maintain that there is  :-(
            movie.setImage(image);
        }


        moviesRepository.save(movie);

        return movie;
    }

    private byte[] resizeImage(byte[] originImage) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(originImage));

        BufferedImage resizedImage = Scalr.resize(bufferedImage, Scalr.Method.BALANCED, Scalr.Mode.AUTOMATIC, provider.getMaxPosterSize(), provider.getMaxPosterSize());

        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(resizedImage, "jpg", arrayOutputStream);
        arrayOutputStream.flush();

        return arrayOutputStream.toByteArray();
    }

    /**
     * Returns one movie entity with nested entities by it identity
     *
     * @param id identity of required movie
     * @return created movie
     */

    public Movie getMovieById(Long id) {
        Movie movie = moviesRepository.findOne(id);
        MovieChecker.checkMovieFound(movie);
        return movie;
    }

    /**
     * Returns list of all movies from DB
     *
     * @return List of movies
     */
    public List< Movie > getMovies() {
        return Lists.newArrayList(moviesRepository.findAll());
    }

    /**
     * Returns one page of movies which ordered by them title
     *
     * @param pageNumber required page number
     * @return Page of movie
     */
    public Page< Movie > getMoviesOrderedByTitle(int pageNumber) {
        return moviesRepository.findAllByOrderByTitle(new PageRequest(pageNumber - 1, provider.getPageSize()));
    }

    /**
     * Returns one page of movies which ordered by year (newest to oldest)
     *
     * @param pageNumber required page number
     * @return Page of movies
     */
    public Page< Movie > getMoviesOrderedByYear(int pageNumber) {
        return moviesRepository.findAllByOrderByYearDesc(new PageRequest(pageNumber - 1, provider.getPageSize()));
    }

    /**
     * Returns one page of movies ordered by them rank (highest to lowest)
     *
     * @param pageNumber required page number
     * @return Page of movies
     */
    public Page< Movie > getMoviesOrderedByRank(int pageNumber) {
        return moviesRepository.findAllByOrderByRank(new PageRequest(pageNumber - 1, provider.getPageSize()));
    }

    /**
     * Adds star to movie. Both entity should be in DB already
     *
     * @param movieId identity of selected movie
     * @param starId  identity of selected star
     * @return updated movie entity
     */
    public Movie addStar(Long movieId, Long starId) {
        Movie movie = moviesRepository.findOne(movieId);
        MovieChecker.checkMovieFound(movie);

        Star star = starsRepository.findOne(starId);
        StarChecker.checkStarFound(star);

        movie.addStar(star);
        moviesRepository.save(movie);

        return movie;
    }

    /**
     * Removes star from movie stars list
     *
     * @param movieId identity of selected movie
     * @param starId  identity od deleting star
     * @return updated movie entity
     */
    public Movie removeStar(Long movieId, Long starId) {
        Movie movie = moviesRepository.findOne(movieId);
        MovieChecker.checkMovieFound(movie);

        Star star = starsRepository.findOne(starId);
        StarChecker.checkStarFound(star);

        movie.removeStar(star);
        moviesRepository.save(movie);

        return movie;
    }

    /**
     * Deletes movie from DB
     *
     * @param id identity of deleting movie
     */
    public void deleteMovie(Long id) {
        Movie movie = moviesRepository.findOne(id);

        MovieChecker.checkMovieFound(movie);

        moviesRepository.delete(id);
    }

    /**
     * Returns movies wish list
     *
     * @param userId identity of authorized user
     * @return List of wish list movies
     */
    public List< Movie > getWishListMovies(Long userId) {
        return Lists.newArrayList(moviesRepository.findAllInWishList(userId));
    }

    /**
     * Returns one page of movies ordered by them title
     *
     * @param userId     identity of authorized user
     * @param pageNumber required page number
     * @return Page of movies
     */
    public Page< Movie > getWishListMoviesOrderedByTitle(int pageNumber, Long userId) {
        return moviesRepository.findAllInWishListOrderByTitle(new PageRequest(pageNumber - 1, provider.getPageSize()), userId);
    }

    /**
     * Returns one page of wish list movies ordered by them year (newest to oldest)
     *
     * @param userId     identity of authorized user
     * @param pageNumber required page number
     * @return Page of movies
     */
    public Page< Movie > getWishListMoviesOrderedByYear(int pageNumber, Long userId) {
        return moviesRepository.findAllInWishListOrderByYear(new PageRequest(pageNumber - 1, provider.getPageSize()), userId);
    }

    /**
     * Returns one page of wish list movies ordered by them ranks (highest to lowest)
     *
     * @param userId     identity of authorized user
     * @param pageNumber required page number
     * @return Page of movies
     */
    public Page< Movie > getWishListMoviesOrderedByRank(int pageNumber, Long userId) {
        return moviesRepository.findAllInWishListOrderByRank(new PageRequest(pageNumber - 1, provider.getPageSize()), userId);
    }

    /**
     * adds movie entity to wish list. Movie should be in DB already
     *
     * @param userId  identity of authorized user
     * @param movieId identity of adding movie
     * @return Movie entity
     */
    public Movie addMovieToBookmarkList(Long movieId, Long userId) {
        Movie movie = moviesRepository.findOne(movieId);
        MovieChecker.checkMovieFound(movie);

        BookmarkListMovie bookmarkListMovie = bookmarkListMoviesRepository.findByMovieIdAndUserId(movieId, userId);
        MovieChecker.checkNotInListMovie(bookmarkListMovie);

        bookmarkListMovie = new BookmarkListMovie(movieId, userId);
        bookmarkListMoviesRepository.save(bookmarkListMovie);

        return removeMovieFromWishList(movieId, userId);
    }

    /**
     * removes movie entity to wish list. Movie should be in DB already
     *
     * @param userId  identity of authorized user
     * @param movieId identity of removing movie
     * @return Movie entity
     */
    public Movie removeMovieFromBookmarkList(Long movieId, Long userId) {
        Movie movie = moviesRepository.findOne(movieId);
        MovieChecker.checkMovieFound(movie);


        BookmarkListMovie bookmarkListMovie = bookmarkListMoviesRepository.findByMovieIdAndUserId(movie.getId(), userId);

        if (bookmarkListMovie != null) {
            bookmarkListMoviesRepository.delete(bookmarkListMovie.getId());
        }

        return movie;
    }

    /**
     * Getter for bookmark list movie
     *
     * @param userId identity of authorized user
     * @return list of movies
     */
    public List< Movie > getBookmarkListMovies(Long userId) {
        return Lists.newArrayList(moviesRepository.findAllInBookmarkListOrderByTitle(userId));
    }

    /**
     * Returns one page of bookmark list movies ordered by them title
     *
     * @param userId     identity of authorized user
     * @param pageNumber required page number
     * @return Page of movies
     */
    public Page< Movie > getBookmarkListMoviesOrderedByTitle(int pageNumber, Long userId) {

        return moviesRepository.findAllInBookmarkListOrderByTitle(new PageRequest(pageNumber - 1, provider.getPageSize()), userId);
    }

    /**
     * Returns one page of bookmark list movies ordered by them year (newest to oldest)
     *
     * @param userId     identity of authorized user
     * @param pageNumber required page number
     * @return Page of movies
     */
    public Page< Movie > getBookmarkListMoviesOrderedByYear(int pageNumber, Long userId) {
        return moviesRepository.findAllInBookmarkListOrderByYear(new PageRequest(pageNumber - 1, provider.getPageSize()), userId);
    }

    /**
     * Returns one page of bookmark list movies ordered by them rank (highest to lowest)
     *
     * @param userId     identity of authorized user
     * @param pageNumber required page number
     * @return Page of movies
     */
    public Page< Movie > getBookmarkListMoviesOrderedByRank(int pageNumber, Long userId) {
        return moviesRepository.findAllInBookmarkListOrderByRank(new PageRequest(pageNumber - 1, provider.getPageSize()), userId);
    }

    /**
     * adds movie entity to bookmark list. Movie should be in DB already
     *
     * @param userId  identity of authorized user
     * @param movieId identity of adding movie
     * @return Movie entity
     */
    public Movie addMovieToWishList(Long movieId, Long userId) {
        Movie movie = moviesRepository.findOne(movieId);
        MovieChecker.checkMovieFound(movie);

        WishListMovie wishListMovie = wishListMoviesRepository.findByMovieIdAndUserId(movieId, userId);
        MovieChecker.checkNotInListMovie(wishListMovie);

        wishListMovie = new WishListMovie(movieId, userId);
        wishListMoviesRepository.save(wishListMovie);

        return removeMovieFromBookmarkList(movieId, userId);
    }

    /**
     * removes movie entity to bookmark list. Movie should be in DB already
     *
     * @param userId  identity of authorized user
     * @param movieId identity of removing movie
     * @return Movie entity
     */
    public Movie removeMovieFromWishList(Long movieId, Long userId) {
        Movie movie = moviesRepository.findOne(movieId);
        MovieChecker.checkMovieFound(movie);

        WishListMovie wishListMovie = wishListMoviesRepository.findByMovieIdAndUserId(movieId, userId);

        if (wishListMovie != null) {
            wishListMoviesRepository.delete(wishListMovie.getId());
        }

        return movie;
    }

    /**
     * Updates movie in DB
     *
     * @param id    identity of updating movie
     * @param movie new movie entity
     * @return updated movie entity
     */
    public Movie updateMovie(Long id, Movie movie) {

        Movie originMovie = moviesRepository.findOne(id);
        MovieChecker.checkMovieFound(originMovie);

        if (!StringUtils.isEmpty(movie.getTitle())) {
            originMovie.setTitle(movie.getTitle());
        }

        if (0 != movie.getYear()) {
            originMovie.setYear(movie.getYear());
        }

        if (null != movie.getImage() && movie.getImage().length != 0) {
            originMovie.setImage(movie.getImage());
        }

        if (!StringUtils.isEmpty(movie.getDescription())) {
            originMovie.setDescription(movie.getDescription());
        }

        moviesRepository.save(originMovie);
        return originMovie;
    }

    /**
     * Adds new movie to DB
     *
     * @param movie movie entity
     * @return created movie entity
     */
    public Movie addMovie(Movie movie) {
        MovieChecker.checkMovieData(movie);

        List< Movie > movieList = moviesRepository.findByTitleAndYear(movie.getTitle(), movie.getYear());
        if (!movieList.isEmpty()) {
            throw new MovieAlreadyExistException();
        }

        moviesRepository.save(movie);
        return movie;
    }

    /**
     * Checks that movie in wish list
     *
     * @param userId  identity of authorized user
     * @param movieId identity of movie
     * @return boolean value with result (true if is it)
     */
    public boolean isMovieInWishList(Long movieId, Long userId) {

        WishListMovie wishListMovie = wishListMoviesRepository.findByMovieIdAndUserId(movieId, userId);
        return (wishListMovie != null);
    }

    /**
     * Check that movie in bookmark list
     *
     * @param userId  identity of authorized user
     * @param movieId identity of movie
     * @return boolean value with result
     */
    public boolean isMovieInBookmarkList(Long movieId, Long userId) {

        BookmarkListMovie bookmarkListMovie = bookmarkListMoviesRepository.findByMovieIdAndUserId(movieId, userId);
        return (bookmarkListMovie != null);
    }

    /**
     * Return list of movie with similar titles
     *
     * @param title title for search
     * @return List of movies
     */
    private List< Movie > searchMoviesByTitle(String title) {
        return moviesRepository.findByTitleStartsWithIgnoreCase(title);
    }

    /**
     * Searches movie by year
     *
     * @param year movie's year
     * @return List with movie
     */
    private List< Movie > searchMoviesByYear(int year) {
        return moviesRepository.findByYear(year);
    }

    /**
     * Searches movies by cascade
     *
     * @param searchKey seach key
     * @return List with found movies
     */
    public List< Movie > cascadeSearchMovie(String searchKey) {
        Set< Movie > movies = new HashSet<>();

        movies.addAll(searchMoviesByTitle(searchKey));

        if (searchKey.matches("[0-9]+")) {
            movies.addAll(searchMoviesByYear(Integer.parseInt(searchKey)));
        }

        return Lists.newArrayList(movies);
    }


    /**
     * Imports movie from OMDB library
     *
     * @param title title of searching movie
     * @return created movie
     */
    public Movie importMovieFromOMBD(String title) {
        try {
            OMDBMovie reachedMovie = ApiFactory.getMovieFromOMDB(title).execute().body();
            MovieChecker.checkOMDBMovieData(reachedMovie);

            OmdbMovieConverter omdbMovieConverter = new OmdbMovieConverter(reachedMovie);

            Movie movie = new Movie(omdbMovieConverter.getMovieYear(), omdbMovieConverter.getMovieTitle(), omdbMovieConverter.getMovieDescription());
            movie.setImage(omdbMovieConverter.getMovieImage());

            MovieChecker.checkMovieData(movie);
            movie = addMovie(movie);

            List< Star > stars = new ArrayList<>(omdbMovieConverter.getMovieStars());

            for (Star currentStar : stars) {
                Star repositoryStar = starsRepository.findByFirstNameAndMiddleNameAndLastNameIgnoreCase(currentStar.getFirstName(), currentStar.getMiddleName(), currentStar.getLastName());
                if (repositoryStar == null) {
                    repositoryStar = starsRepository.save(currentStar);
                }
                addStar(movie.getId(), repositoryStar.getId());
            }

            if (null != omdbMovieConverter.getMovieRank()) {
                ranksRepository.save(new Rank(movie, omdbMovieConverter.getMovieRank().getRank()));
            }

            return movie;
        } catch (IOException e) {
            throw new OMDBImportException();
        }
    }

    /**
     * Checks is movie exists in OMDB library
     *
     * @param title required movie title
     * @return boolean result
     */
    public boolean isMovieExistsInOmdb(String title) {
        try {
            return ApiFactory.getMovieFromOMDB(title).execute().body().getTitle() != null;
        } catch (IOException e) {
            throw new OMDBImportException();
        }
    }

    /**
     * Returns page of top movies, which was liked much all
     *
     * @return List of movies
     */
    public List< Movie > getTopMovies() {
        List< Movie > movies = moviesRepository.findTopMovies();
        return movies.stream().limit(provider.getPageSize()).collect(Collectors.toList());
    }

    /**
     * Returns page of newest movies, which was added recently
     *
     * @return List of movies
     */
    public List< Movie > getRecentMovies() {
        List< Movie > movies = moviesRepository.findRecentMovies();
        return movies.stream().limit(provider.getPageSize()).collect(Collectors.toList());
    }
}
