package net.thumbtack.cinematheque.services;

import com.google.common.collect.Lists;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.exceptions.checkers.MovieChecker;
import net.thumbtack.cinematheque.exceptions.checkers.RanksChecker;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import net.thumbtack.cinematheque.repositories.RanksRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service layer for ranks functional
 */

@Service
public class RankService {
    private final Logger logger = LoggerFactory.getLogger(RankService.class);

    @Autowired
    private RanksRepository ranksRepository;

    @Autowired
    private MoviesRepository moviesRepository;

    /**
     * Returns rank list for every ranked movie
     *
     * @return ranks list
     */
    public List< Rank > getRanks() {
        return Lists.newArrayList(ranksRepository.findAll());
    }

    /**
     * Returns one rank by identity
     *
     * @param id identity of required rank
     * @return Rank entity
     */
    public Rank getRankById(Long id) {
        Rank rank = ranksRepository.findOne(id);
        RanksChecker.checkRankFound(rank);
        return rank;
    }

    /**
     * Adds rank to DB
     *
     * @param movieId identity of ranked movie
     * @param value   rank value, positive number between 1 and 5
     * @return Created rank entity with identity
     */
    public Rank addRank(Long movieId, int value) {
        Movie movie = moviesRepository.findOne(movieId);

        MovieChecker.checkMovieFound(movie);
        RanksChecker.movieAlreadyRanked(movie);

        Rank rank = new Rank(movie, value);
        RanksChecker.checkRankData(rank);
        ranksRepository.save(rank);
        return rank;
    }

    /**
     * Universal method for adding or updating rank if it in DB already. Just wrapper
     *
     * @param movieId identity of movie
     * @param value   new rank value
     */
    public void addOrUpdateRank(Long movieId, int value) {
        Movie movie = moviesRepository.findOne(movieId);
        MovieChecker.checkMovieFound(movie);

        if (null == movie.getRank()) {
            addRank(movieId, value);
        } else {
            movie.getRank().setRank(value);
            updateRank(movie.getRank().getId(), movie.getRank());
        }
    }

    /**
     * Updates rank value in DB
     *
     * @param id   identity of rank which in DB already
     * @param rank Entity with new data
     * @return updated rank entity
     */
    public Rank updateRank(Long id, Rank rank) {
        Rank originRank = ranksRepository.findOne(id);

        RanksChecker.checkRankFound(originRank);

        if (0 != rank.getRank()) {
            originRank.setRank(rank.getRank());
        }

        ranksRepository.save(originRank);
        return originRank;
    }
}
