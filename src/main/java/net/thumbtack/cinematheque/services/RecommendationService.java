package net.thumbtack.cinematheque.services;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.listmovies.RecommendedListMovie;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import net.thumbtack.cinematheque.repositories.RecommendedMoviesRepository;
import net.thumbtack.cinematheque.repositories.WishListMoviesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service layer for recommendation functional
 */

@Service
public class RecommendationService {

    static final double MAXIMAL_INTERESTED_SIMILARITY = 0.95;
    static final double OPTIMAL_SIMILARITY_TOP = 0.85;
    static final double OPTIMAL_SIMILARITY_BOTTOM = 0.65;

    @Autowired
    private WishListMoviesRepository wishListMoviesRepository;

    @Autowired
    private MoviesRepository moviesRepository;

    @Autowired
    private RecommendedMoviesRepository recommendedMoviesRepository;

    private Logger logger = LoggerFactory.getLogger(RecommendationService.class);

    /**
     * Updates list with personal recommendations based on wish lists of other user with maximal similarity
     *
     * @param userId identity of required user
     * @return List with movie recommendations
     */
    public List< Movie > updateRecommendationsList(Long userId) {
        List< Movie > recommendationsList = buildRecommendationsList(userId);
        clearRecommendationsList(userId);
        saveRecommendationsList(userId, recommendationsList);

        return recommendationsList;
    }

    /**
     * Getter for cached recommendations list
     *
     * @param userId identity of user
     * @return List with recommendations
     */
    public List< Movie > getRecommendationsList(Long userId) {
        List< Movie > recommendationList = moviesRepository.findAllRecommendedMovie(userId);

        if (recommendationList.isEmpty()) {
            recommendationList = updateRecommendationsList(userId);
        }

        if (recommendationList.isEmpty()) {
            recommendationList = moviesRepository.findTopMovies();
        }

        if (recommendationList.isEmpty()) {
            recommendationList = moviesRepository.findRecentMovies();
        }

        return recommendationList;
    }

    private List< Movie > buildRecommendationsList(Long userId) {
        Double maximalSimilarity = 0D;
        Long maximalSimilarityUserId = null;

        List< Long > activeUsers = wishListMoviesRepository.findActiveUsers();
        activeUsers.remove(userId);

        for (Long currentUserId : activeUsers) {
            double currentSimilarity = getMoviesListSimilarity(currentUserId, userId);

            if (currentSimilarity <= OPTIMAL_SIMILARITY_TOP && currentSimilarity >= OPTIMAL_SIMILARITY_BOTTOM) {
                maximalSimilarityUserId = currentUserId;
                break;
            }

            if (currentSimilarity >= maximalSimilarity && currentSimilarity <= MAXIMAL_INTERESTED_SIMILARITY) {
                maximalSimilarityUserId = currentUserId;
                maximalSimilarity = currentSimilarity;
            }
        }
        return wishListMoviesRepository.getWishListDifference(maximalSimilarityUserId, userId);
    }

    private void clearRecommendationsList(Long userId) {
        recommendedMoviesRepository.deleteByUserId(userId);
    }

    private void saveRecommendationsList(Long userId, List< Movie > recommendationsMovieList) {

        List< RecommendedListMovie > recommendationList = recommendationsMovieList.stream().map(movie -> new RecommendedListMovie(movie.getId(), userId)).collect(Collectors.toCollection(LinkedList::new));
        recommendedMoviesRepository.save(recommendationList);

    }

    private Double getMoviesListSimilarity(Long firstUser, Long secondUser) {
        int intersectionMovies = wishListMoviesRepository.getUsersIntersectionCount(firstUser, secondUser);
        return intersectionMovies / (double) wishListMoviesRepository.getUserWishListSize(firstUser);
    }

}
