package net.thumbtack.cinematheque.services;


import com.google.common.collect.Lists;
import net.thumbtack.cinematheque.config.ApplicationConfigurationProvider;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.exceptions.checkers.StarChecker;
import net.thumbtack.cinematheque.exceptions.starsexceptions.StarAlreadyExistsException;
import net.thumbtack.cinematheque.repositories.StarsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service layer for stars functional
 */


@Service
public class StarService {
    private final Logger logger = LoggerFactory.getLogger(StarService.class);

    @Autowired
    private ApplicationConfigurationProvider provider;

    @Autowired
    private StarsRepository starsRepository;

    /**
     * Adds star to DB
     *
     * @param star Star entity
     * @return Created star entity
     */
    public Star addStar(Star star) {
        StarChecker.checkStarData(star);

        List< Star > starList = starsRepository.findByFirstNameAndLastNameIgnoreCase(star.getFirstName(), star.getLastName());
        if (!starList.isEmpty()) {
            throw new StarAlreadyExistsException();
        }

        return starsRepository.save(star);
    }

    /**
     * Deletes star from DB by identity
     *
     * @param id Star identity
     */
    public void deleteStar(Long id) {
        Star star = starsRepository.findOne(id);
        StarChecker.checkStarFound(star);

        starsRepository.delete(star);
    }

    /**
     * Returns list with all stars in DB
     *
     * @param pageNumber number of page
     * @return Stars list
     */
    public Page< Star > getStars(int pageNumber) {
        return starsRepository.findAllByOrderByFirstName(new PageRequest(pageNumber - 1, provider.getPageSize()));
    }

    /**
     * Returns all stars ordered by First name
     *
     * @return List of stars
     */
    public List< Star > getStars() {
        return starsRepository.findAll();
    }

    /**
     * Returns one selected star from DB
     *
     * @param id identity of required star
     * @return Star entity
     */
    public Star getStarById(Long id) {
        Star star = starsRepository.findOne(id);
        StarChecker.checkStarFound(star);

        return starsRepository.findOne(id);
    }

    /**
     * Returns list of stars by them last names
     *
     * @param lastName last name of required stars
     * @return List of stars
     */
    public List< Star > getStarsByLastName(String lastName) {
        return Lists.newArrayList(starsRepository.findByLastNameIgnoreCase(lastName));
    }

    /**
     * Returns list of stars by them first names
     *
     * @param firstName first name of required stars
     * @return List of stars
     */
    public List< Star > getByFirstName(String firstName) {
        return Lists.newArrayList(starsRepository.findByFirstNameIgnoreCase(firstName));
    }

    /**
     * Updates star in DB
     *
     * @param id   identity of updating star
     * @param star actual stars data
     * @return Updated star entity
     */

    public Star updateStar(Long id, Star star) {
        Star originStar = starsRepository.findOne(id);
        StarChecker.checkStarFound(originStar);

        if (!StringUtils.isEmpty(star.getLastName())) {
            originStar.setLastName(star.getLastName());
        }

        if (!StringUtils.isEmpty(star.getFirstName())) {
            originStar.setFirstName(star.getFirstName());
        }

        if (!StringUtils.isEmpty(star.getMiddleName())) {
            originStar.setMiddleName(star.getMiddleName());
        }

        starsRepository.save(originStar);

        return originStar;
    }

    /**
     * Returns stars which has defined last name
     *
     * @param lastName last name of star
     * @return List of stars
     */
    private List< Star > searchStarsByLastName(String lastName) {
        return Lists.newArrayList(starsRepository.findByLastNameStartsWithIgnoreCase(lastName));
    }

    /**
     * Returns stars which has defined first name
     *
     * @param firstName last name of star
     * @return List of stars
     */
    private List< Star > searchStarsByFirstName(String firstName) {
        return Lists.newArrayList(starsRepository.findByFirstNameIgnoreCase(firstName));
    }

    /**
     * Returns stars which has defined first name
     *
     * @param middleName last name of star
     * @return List of stars
     */
    private List< Star > searchStarsByMiddleName(String middleName) {
        return Lists.newArrayList(starsRepository.findByMiddleNameIgnoreCase(middleName));
    }

    /**
     * Returns stars where at least one field looks like to search phrase
     *
     * @param searchKey key phrase for search
     * @return List with stars
     */

    public List< Star > cascadeSearchStar(String searchKey) {
        Set< Star > stars = new HashSet<>();
        stars.addAll(searchStarsByLastName(searchKey));
        stars.addAll(searchStarsByFirstName(searchKey));
        stars.addAll(searchStarsByMiddleName(searchKey));

        return Lists.newArrayList(stars);
    }
}
