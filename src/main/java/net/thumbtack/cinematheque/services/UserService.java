package net.thumbtack.cinematheque.services;

import net.thumbtack.cinematheque.config.ApplicationConfigurationProvider;
import net.thumbtack.cinematheque.repositories.MoviesRepository;
import net.thumbtack.cinematheque.repositories.WishListMoviesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service layer for users functional
 */

@Service
public class UserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private ApplicationConfigurationProvider applicationConfigurationProvider;

    @Autowired
    private WishListMoviesRepository wishListMoviesRepository;

    @Autowired
    private MoviesRepository moviesRepository;

    /**
     * Returns user roles for OAuth user
     *
     * @param userId user identity
     * @return List with roles
     */
    public List< String > getOauthUserRoles(Long userId) {
        List< String > result = new ArrayList<>();
        result.add(applicationConfigurationProvider.getDefaultMemberRole());

        if (applicationConfigurationProvider.isUserIsAdmin(userId)) {
            result.add(applicationConfigurationProvider.getAdminMemberRole());
        }

        return result;
    }

    /**
     * Returns list of user id who liked at least one movie
     *
     * @return list with user identities
     */
    public List< Long > getActiveUsers() {
        return wishListMoviesRepository.findActiveUsers();
    }
}
