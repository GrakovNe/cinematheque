package net.thumbtack.cinematheque.omdb.entity;

import com.google.gson.annotations.SerializedName;

/**
 * OMDB Movie entity. Looks like DTO, but not
 */
public class OMDBMovie {
    @SerializedName("Title")
    private String title;

    @SerializedName("Year")
    private String year;

    @SerializedName("Plot")
    private String plot;

    @SerializedName("imdbRating")
    private String imdbRating;

    @SerializedName("Actors")
    private String actors;

    @SerializedName("Poster")
    private String poster;

    /**
     * Empty class constructor
     */
    public OMDBMovie() {
    }

    /**
     * Getter for title
     *
     * @return String with movie title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter for movie year
     *
     * @return string of movie year
     */
    public String getYear() {
        return year;
    }

    /**
     * Getter for movie poster
     *
     * @return string with poster URL
     */
    public String getPoster() {
        return poster;
    }

    /**
     * Getter of ImDB rating
     *
     * @return string with imdb rating
     */
    public String getImdbRating() {
        return imdbRating;
    }

    /**
     * Getter for stars of movie
     *
     * @return movie stars string
     */
    public String getActors() {
        return actors;
    }

    /**
     * Getter for movie plot
     *
     * @return String with movie plot
     */
    public String getPlot() {
        return plot;
    }

}
