package net.thumbtack.cinematheque.omdb.converters;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.exceptions.moviesexceptions.OMDBImportException;
import net.thumbtack.cinematheque.omdb.entity.OMDBMovie;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Converts reached by network OMDB Movie to Just Plain Movie
 */

public class OmdbMovieConverter {
    private static final String WITHOUT_PREFIX = "N/A";
    private static final String URL_PREFIX = "http://";
    private static final String YEAR_SEARCH_MASK = "[^-?0-9]+";
    private static final int MAX_YEAR_NUMBERS = 4;
    private Movie movie = new Movie();

    /**
     * Public constructor. All converts here and immediately
     *
     * @param omdbMovie movie for convert
     */
    public OmdbMovieConverter(OMDBMovie omdbMovie) {
        if (null != omdbMovie.getTitle()) {
            movie.setTitle(omdbMovie.getTitle());
        }

        if (null != omdbMovie.getYear() && !omdbMovie.getYear().equals(WITHOUT_PREFIX)) {
            movie.setYear(processYear(omdbMovie.getYear()));
        }

        if (null != omdbMovie.getActors()) {
            movie.setStars(new HashSet<>(convertStars(omdbMovie.getActors())));
        }

        if (null != omdbMovie.getPlot() && !omdbMovie.getPlot().equals(WITHOUT_PREFIX)) {
            movie.setDescription(omdbMovie.getPlot());
        }

        if (null != omdbMovie.getPoster() && omdbMovie.getPoster().startsWith(URL_PREFIX)) {

            try {
                URL url = new URL(omdbMovie.getPoster());
                movie.setImage(loadOMDBImage(url));

            } catch (IOException e) {
                throw new OMDBImportException();
            }

        }

        try {
            movie.setRank(new Rank(movie, (int) Math.round(Float.parseFloat(omdbMovie.getImdbRating()) / 2.0)));
        } catch (NumberFormatException ex) {
            movie.setRank(null);
        }
    }

    private static List< Star > convertStars(String actors) {
        String[] rawStarArray = actors.split(", ");
        List< Star > stars = new ArrayList<>();

        if (rawStarArray.length == 0) {
            return new ArrayList<>();
        }

        for (String rawStar : rawStarArray) {
            String[] starName = rawStar.split(" ");

            Star currentStar = new Star();

            switch (starName.length) {
                case 1:
                    currentStar.setFirstName("");
                    currentStar.setLastName(starName[ 0 ]);
                    break;

                case 2:
                    currentStar.setFirstName(starName[ 0 ]);
                    currentStar.setLastName(starName[ 1 ]);
                    break;

                case 3:
                    currentStar.setFirstName(starName[ 0 ]);
                    currentStar.setMiddleName(starName[ 1 ]);
                    currentStar.setLastName(starName[ 2 ]);
                    break;
            }

            stars.add(currentStar);
        }

        return stars;
    }

    private byte[] loadOMDBImage(URL url) throws IOException {
        return IOUtils.toByteArray(url.openStream());
    }

    /**
     * Getter for movie identity
     *
     * @return movie ID
     */
    public Long getMovieId() {
        return movie.getId();
    }

    /**
     * Getter for movie rank
     *
     * @return Rank entity
     */
    public Rank getMovieRank() {
        return movie.getRank();
    }

    /**
     * Getter for stars in a movie
     *
     * @return Set of Stars
     */
    public Set< Star > getMovieStars() {
        return movie.getStars();
    }

    /**
     * Getter for movie year
     *
     * @return int, year of movie was made
     */
    public int getMovieYear() {
        return movie.getYear();
    }

    /**
     * Getter for movie title
     *
     * @return Title of movie
     */
    public String getMovieTitle() {
        return movie.getTitle();
    }

    /**
     * Getter for movie image
     *
     * @return byte array with picture
     */
    public byte[] getMovieImage() {
        return movie.getImage();
    }

    /**
     * Getter for movie description
     *
     * @return String with movie description
     */
    public String getMovieDescription() {
        return movie.getDescription();
    }

    private Integer processYear(String rawYear) {
        return Integer.parseInt(rawYear.replaceAll(YEAR_SEARCH_MASK, "").substring(0, MAX_YEAR_NUMBERS));
    }
}
