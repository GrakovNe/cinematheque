package net.thumbtack.cinematheque.omdb.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.thumbtack.cinematheque.omdb.entity.OMDBMovie;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * API factory for Retrofit
 */
public abstract class ApiFactory {
    private static final String BASE_URL = "http://www.omdbapi.com/";

    private static final Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    /**
     * Getter fot omdb instance
     *
     * @return Retrofit class instance
     */
    public static Retrofit getRetrofit() {
        return retrofit;
    }

    /**
     * Makes call for movie to OMDB
     *
     * @param title title of searched movie
     * @return Call with query
     */
    public static Call< OMDBMovie > getMovieFromOMDB(String title) {
        OMDBApi api = ApiFactory.getRetrofit().create(OMDBApi.class);

        return api.getMovies(title);
    }


}
