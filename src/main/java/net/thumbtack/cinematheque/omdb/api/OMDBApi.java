package net.thumbtack.cinematheque.omdb.api;

import net.thumbtack.cinematheque.omdb.entity.OMDBMovie;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Public api methods for import movie from OMDB
 */
public interface OMDBApi {

    /**
     * Makes request to OMDB library
     *
     * @param title required title
     * @return Call with request data
     */

    @GET("/")
    Call< OMDBMovie > getMovies(@Query("t") String title);
}
