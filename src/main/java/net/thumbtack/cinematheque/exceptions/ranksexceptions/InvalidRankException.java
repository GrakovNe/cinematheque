package net.thumbtack.cinematheque.exceptions.ranksexceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on invalid data of rank (such as nulls or empty fields)
 */

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Invalid rank data")
public class InvalidRankException extends RuntimeException {
}
