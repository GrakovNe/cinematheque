package net.thumbtack.cinematheque.exceptions.ranksexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on required rank isn't in DB
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Rank not found")
public class RankNotFoundException extends RuntimeException {
}
