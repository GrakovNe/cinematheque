package net.thumbtack.cinematheque.exceptions.ranksexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Exception on ranking movie was ranked before
 */

@ResponseStatus(value = HttpStatus.FOUND, reason = "Movie already ranked")
public class MovieAlreadyRankedException extends RuntimeException {
}
