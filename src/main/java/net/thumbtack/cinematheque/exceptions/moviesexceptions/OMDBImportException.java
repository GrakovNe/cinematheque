package net.thumbtack.cinematheque.exceptions.moviesexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception during OMDB import
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Movie not found in the OMDB")
public class OMDBImportException extends RuntimeException {
}
