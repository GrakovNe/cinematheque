package net.thumbtack.cinematheque.exceptions.moviesexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on requested movie's image isn't found
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Image not found")
public class ImageNotFoundException extends RuntimeException {
}
