package net.thumbtack.cinematheque.exceptions.moviesexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on invalid data of movie (such as nulls or empty fields)
 */
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Incorrect movie data")
public class InvalidMovieException extends RuntimeException {
}
