package net.thumbtack.cinematheque.exceptions.moviesexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on same Movie is already into DB
 */
@ResponseStatus(value = HttpStatus.FOUND, reason = "Movie already exists")
public class MovieAlreadyExistException extends RuntimeException {
}
