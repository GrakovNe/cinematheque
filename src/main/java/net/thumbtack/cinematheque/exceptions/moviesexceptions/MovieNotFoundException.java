package net.thumbtack.cinematheque.exceptions.moviesexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on required movie is not in DB
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Movie")
public class MovieNotFoundException extends RuntimeException {
}
