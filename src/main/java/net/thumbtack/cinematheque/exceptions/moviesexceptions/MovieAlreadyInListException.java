package net.thumbtack.cinematheque.exceptions.moviesexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on movie already in some list
 */
@ResponseStatus(value = HttpStatus.FOUND, reason = "Movie already in list")
public class MovieAlreadyInListException extends RuntimeException {
}
