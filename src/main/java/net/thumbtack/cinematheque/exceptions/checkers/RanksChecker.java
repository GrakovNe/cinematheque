package net.thumbtack.cinematheque.exceptions.checkers;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Rank;
import net.thumbtack.cinematheque.exceptions.ranksexceptions.InvalidRankException;
import net.thumbtack.cinematheque.exceptions.ranksexceptions.MovieAlreadyRankedException;
import net.thumbtack.cinematheque.exceptions.ranksexceptions.RankNotFoundException;

/**
 * Static class for validating Ranks Entity
 */

public class RanksChecker {
    /**
     * Checks that movie wasn't ranked before
     *
     * @param movie movie entity
     */
    public static void movieAlreadyRanked(Movie movie) {
        if (movie.getRank() != null) {
            throw new MovieAlreadyRankedException();
        }
    }

    /**
     * Checks that rank isn't null
     *
     * @param rank rank movie
     */
    public static void checkRankFound(Rank rank) {
        if (null == rank) {
            throw new RankNotFoundException();
        }
    }

    /**
     * Check all field of rank for correctness
     *
     * @param rank rank entity
     */
    public static void checkRankData(Rank rank) {
        if (null == rank || rank.getRank() < 1 || rank.getRank() > 5 || null == rank.getMovieId()) {
            throw new InvalidRankException();
        }
    }
}
