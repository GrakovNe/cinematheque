package net.thumbtack.cinematheque.exceptions.checkers;

import net.thumbtack.cinematheque.entity.Star;
import net.thumbtack.cinematheque.exceptions.starsexceptions.InvalidStarException;
import net.thumbtack.cinematheque.exceptions.starsexceptions.StarNotFoundException;
import org.springframework.util.StringUtils;

/**
 * Static class for validating Stars entity
 */

public class StarChecker {

    /**
     * Check that star isn't null
     *
     * @param star star entity
     */
    public static void checkStarFound(Star star) {
        if (null == star) {
            throw new StarNotFoundException();
        }
    }

    /**
     * Checks all star fields for correctness
     *
     * @param star start entity
     */
    public static void checkStarData(Star star) {
        if (null == star || StringUtils.isEmpty(star.getFirstName()) || StringUtils.isEmpty(star.getLastName())) {
            throw new InvalidStarException();
        }
    }
}
