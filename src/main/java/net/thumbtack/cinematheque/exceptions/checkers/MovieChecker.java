package net.thumbtack.cinematheque.exceptions.checkers;

import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.interfaces.ListMovie;
import net.thumbtack.cinematheque.entity.listmovies.WishListMovie;
import net.thumbtack.cinematheque.exceptions.moviesexceptions.*;
import net.thumbtack.cinematheque.omdb.entity.OMDBMovie;
import org.springframework.util.StringUtils;

/**
 * Static class for validate movies entity
 */

public class MovieChecker {
    private static final int MINIMAL_MOVIE_YEAR = 1895;

    /**
     * Checks that movie is not null
     *
     * @param movie movie entity
     */
    public static void checkMovieFound(Movie movie) {
        if (null == movie) {
            throw new MovieNotFoundException();
        }
    }

    /**
     * Checks that WishListMovie entity is not null
     *
     * @param wishListMovie wish list movie entity
     */
    public static void checkWishListMovie(WishListMovie wishListMovie) {
        if (null == wishListMovie) {
            throw new MovieNotFoundException();
        }
    }

    /**
     * Checks that ListMovie is not null
     *
     * @param listMovie List Movie entity
     */
    public static void checkNotInListMovie(ListMovie listMovie) {
        if (listMovie != null) {
            throw new MovieAlreadyInListException();
        }
    }

    /**
     * Checks that movie was successfully imported from OMDB
     *
     * @param omdbMovie imported movie
     */
    public static void checkOMDBMovieData(OMDBMovie omdbMovie) {
        if (null == omdbMovie || null == omdbMovie.getTitle()) {
            throw new OMDBImportException();
        }
    }

    /**
     * Checks that bytes array isn't null
     *
     * @param bytes byte array with picture
     */
    public static void checkMovieImageFound(byte[] bytes) {
        if (null == bytes || bytes.length == 0) {
            throw new ImageNotFoundException();
        }
    }

    /**
     * Check movie data and throws exception if one of them is'n correct
     *
     * @param movie movie entity
     */
    public static void checkMovieData(Movie movie) {
        if (null == movie || StringUtils.isEmpty(movie.getTitle()) || movie.getYear() < MINIMAL_MOVIE_YEAR) {
            throw new InvalidMovieException();
        }
    }


}
