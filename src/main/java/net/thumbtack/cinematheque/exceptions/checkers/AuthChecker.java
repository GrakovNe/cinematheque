package net.thumbtack.cinematheque.exceptions.checkers;

import net.thumbtack.cinematheque.exceptions.sessionexceptions.UnauthorizedException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * Static class for validate user auth
 */
public class AuthChecker {
    /**
     * Check that received user is not null
     *
     * @param user user entity
     */
    public static void checkAuth(OAuth2Authentication user) {
        if (null == user) {
            throw new UnauthorizedException();
        }
    }
}
