package net.thumbtack.cinematheque.exceptions.starsexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on required Star is't in DB
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Star not found")
public class StarNotFoundException extends RuntimeException {
}
