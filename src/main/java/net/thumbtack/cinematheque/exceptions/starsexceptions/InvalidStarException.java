package net.thumbtack.cinematheque.exceptions.starsexceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on invalid data of movie (such as nulls or empty fields)
 */

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Invalid star data")
public class InvalidStarException extends RuntimeException {
}
