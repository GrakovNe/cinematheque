package net.thumbtack.cinematheque.exceptions.starsexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on same star already exist in DB
 */
@ResponseStatus(value = HttpStatus.FOUND, reason = "Star already exists")
public class StarAlreadyExistsException extends RuntimeException {
}
