package net.thumbtack.cinematheque;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Backend starter for application. Main Class here
 */

@SpringBootApplication
@EnableScheduling
public class Application {
    /**
     * Entry point application class
     *
     * @param args console arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
