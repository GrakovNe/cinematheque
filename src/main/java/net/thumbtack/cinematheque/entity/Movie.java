package net.thumbtack.cinematheque.entity;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.thumbtack.cinematheque.serializers.CustomMovieSerializer;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Movie entity class.
 */

@Entity
@Table(name = "movies")
@JsonSerialize(using = CustomMovieSerializer.class)
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String description;

    private int year;

    @Basic(fetch = FetchType.LAZY)
    private byte[] image;

    @OneToOne(mappedBy = "movie", cascade = CascadeType.ALL)

    private Rank rank;
    @ManyToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinTable(
            name = "movies_stars",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "star_id")
    )


    //@JsonSerialize(using = CustomStarSerializer.class)
    private Set< Star > stars = new HashSet<>();

    /**
     * empty constructor, it needs for spring JPA access
     */
    public Movie() {

    }

    /**
     * Public constructor for creating movie with data
     *
     * @param year        year of movie was made
     * @param description short movie description or plot
     * @param title       title of movie
     */
    public Movie(int year, String title, String description) {
        this.year = year;
        this.title = title;
        this.description = description;
    }

    /**
     * Getter for movie poster
     *
     * @return byte array with picture
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Setter for movie poster
     *
     * @param image byte array with picture
     */
    public void setImage(byte[] image) {
        this.image = image;
    }

    /**
     * Getter for ID
     *
     * @return identity of movie in DB
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter for ID
     *
     * @param id identity of movie in DB
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter for title
     *
     * @return title of movie
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets new title to movie
     *
     * @param title new movie title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter for movie description
     *
     * @return movie description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for a movie description
     *
     * @param description string with description of movie
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for year
     *
     * @return year of movie
     */
    public int getYear() {
        return year;
    }

    /**
     * Sets new year for movie was made
     *
     * @param year new year
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * Getter for stars list
     *
     * @return List with stars if it in DB or empty list else
     */
    public Set< Star > getStars() {
        return stars;
    }

    /**
     * Setter for stars set
     *
     * @param stars set of stars attached to movie
     */
    public void setStars(Set< Star > stars) {
        this.stars = stars;
    }

    /**
     * Getter for rank
     *
     * @return rank entity or null if doesn't ranked before
     */
    public Rank getRank() {
        return rank;
    }

    /**
     * Setter for rank of movie
     *
     * @param rank Rank entity attached to movie
     */
    public void setRank(Rank rank) {
        this.rank = rank;
    }

    /**
     * Adds star entity to stars list
     *
     * @param star Star entity
     */
    public void addStar(Star star) {
        stars.add(star);
    }

    /**
     * Removes star from stars list
     *
     * @param star Star entity
     */
    public void removeStar(Star star) {
        stars.remove(star);
    }

    /**
     * Prints data for Movie and nested entities
     *
     * @return String with movie data
     */


    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", rank=" + rank +
                ", stars=" + stars +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        return year == movie.year && title.equals(movie.title) && (description != null ? description.equals(movie.description) : movie.description == null && Arrays.equals(image, movie.image) && (stars != null ? stars.equals(movie.stars) : movie.stars == null));

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + Arrays.hashCode(image);
        result = 31 * result + (stars != null ? stars.hashCode() : 0);
        return result;
    }
}
