package net.thumbtack.cinematheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Rank entity class
 */

@Entity
@Table(name = "ranks")
public class Rank {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JsonIgnore
    @JoinColumn(name = "movie_id")
    private Movie movie;

    private int rank;

    /**
     * Public constructor.
     *
     * @param movie ranking movie entity
     * @param rank  rank value. Positive integer between 1 and 5
     */
    public Rank(Movie movie, int rank) {
        this.movie = movie;
        this.rank = rank;
    }

    /**
     * Empty constructor, needs for spring JPA
     */
    public Rank() {
    }

    /**
     * Getter for Identity
     *
     * @return Long number with Identity of rank in DB
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter for identity
     *
     * @param id Long number with Identity of rank in DB
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter for id of ranked movie
     *
     * @return Long number with identity of ranked movie
     */
    public Long getMovieId() {
        return movie.getId();
    }

    /**
     * Getter for rank value
     *
     * @return positive int value between 1 and 5
     */
    public int getRank() {
        return rank;
    }

    /**
     * Setter for movie rank
     *
     * @param rank positive integer beetwen 1 and 5
     */
    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "Rank{" +
                "id=" + id +
                ", movie=" + movie.getId() +
                ", rank=" + rank +
                '}';
    }

    /**
     * prints Rank data
     *
     * @return String with data
     */


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rank rank1 = (Rank) o;

        if (rank != rank1.rank) return false;
        return movie != null ? movie.equals(rank1.movie) : rank1.movie == null;

    }

    @Override
    public int hashCode() {
        int result = movie != null ? movie.hashCode() : 0;
        result = 31 * result + rank;
        return result;
    }
}
