package net.thumbtack.cinematheque.entity;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.Map;

/**
 * User entity for Oauth2 from Facebook
 */
public class FaceBookUser {
    private Long id;

    private String userName;

    private String realName;

    /**
     * Class constructor which makes Enity from OAuth2Authentication in request
     *
     * @param oAuth2Authentication oAuth2Authentication entity from request
     */
    public FaceBookUser(OAuth2Authentication oAuth2Authentication) {
        @SuppressWarnings("unchecked")
        Map< String, String > userData = (Map< String, String >) oAuth2Authentication.getUserAuthentication().getDetails();

        this.id = Long.valueOf(userData.get("id"));
        this.userName = userData.get("id");
        this.realName = userData.get("name");
    }

    /**
     * Getter for user identity
     *
     * @return Long with user id
     */
    public Long getId() {
        return id;
    }

    /**
     * Getter for user name (the same as id but string)
     *
     * @return String with username
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Getter for user profile name
     *
     * @return String with user name
     */
    public String getRealName() {
        return realName;
    }

    @Override
    public String toString() {
        return "FaceBookUser{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", realName='" + realName + '\'' +
                '}';
    }
}
