package net.thumbtack.cinematheque.entity.listmovies;

import net.thumbtack.cinematheque.entity.interfaces.ListMovie;

import javax.persistence.*;

/**
 * class which says that movie in bookmark list
 */

@Entity
@Table(name = "movies_bookmarked")
public class BookmarkListMovie implements ListMovie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long movieId;
    private Long userId;

    /**
     * Empty movie constructor. Needs for JPA
     */
    public BookmarkListMovie() {
    }

    /**
     * Public constructor. Needs to create bookmark list movie with data
     *
     * @param userId  identity of user which added movie to list
     * @param movieId identity of movie
     */
    public BookmarkListMovie(Long movieId, Long userId) {
        this.movieId = movieId;
        this.userId = userId;
    }

    /**
     * Getter for bookmark list movie identity
     *
     * @return bookmark list movie identity
     */
    public Long getId() {
        return id;
    }

    /**
     * Getter for movie identity
     *
     * @return identity of original movie
     */
    public Long getMovieId() {
        return movieId;
    }
}
