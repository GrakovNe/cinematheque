package net.thumbtack.cinematheque.entity.listmovies;

import net.thumbtack.cinematheque.entity.interfaces.ListMovie;

import javax.persistence.*;

/**
 * class which says that movie in wish list
 */

@Entity
@Table(name = "movies_wishlist")
public class WishListMovie implements ListMovie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long movieId;
    private Long userId;

    /**
     * Empty constructor. Needs for JPA
     */
    public WishListMovie() {
    }

    /**
     * Public constructor.
     *
     * @param movieId identity of movie which in DB already
     * @param userId  identity of authorized user
     */
    public WishListMovie(Long movieId, Long userId) {
        this.movieId = movieId;
        this.userId = userId;
    }

    /**
     * Getter for self identity
     *
     * @return identity
     */
    public Long getId() {
        return id;
    }

    /**
     * Getter for movie identity
     *
     * @return movie identity
     */
    public Long getMovieId() {
        return movieId;
    }
}
