package net.thumbtack.cinematheque.entity.listmovies;

import net.thumbtack.cinematheque.entity.interfaces.ListMovie;

import javax.persistence.*;

/**
 * Class which says that movie is recommended for user
 */

@Entity
@Table(name = "movies_recommended")
public class RecommendedListMovie implements ListMovie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long movieId;
    private Long userId;

    /**
     * Empty class constructor. It needs for JPA
     */
    public RecommendedListMovie() {
    }

    /**
     * Public class constructor
     *
     * @param movieId identity of movie
     * @param userId  identity of user from wish list
     */
    public RecommendedListMovie(Long movieId, Long userId) {
        this.movieId = movieId;
        this.userId = userId;
    }

    /**
     * Getter for identity
     *
     * @return Long with identity
     */

    public Long getId() {
        return id;
    }

    /**
     * Getter for movie identity
     *
     * @return Long with movie identity
     */
    public Long getMovieId() {
        return movieId;
    }

    /**
     * Getter for user identity
     *
     * @return long with user identity
     */
    public Long getUserId() {
        return userId;
    }

}
