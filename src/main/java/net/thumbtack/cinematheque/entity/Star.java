package net.thumbtack.cinematheque.entity;

import javax.persistence.*;

/**
 * Star entity class
 */

@Entity
@Table(name = "stars")
public class Star {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String middleName;
    private String lastName;

    /**
     * Public constructor
     *
     * @param firstName  first name of star
     * @param middleName middle name of star
     * @param lastName   last name of star
     */
    public Star(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    /**
     * Empty constructor needs for spring JPA only
     */
    public Star() {
    }

    /**
     * Public constructor
     *
     * @param firstName first name of star
     * @param lastName  last name of star
     */
    public Star(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Getter for Identity
     *
     * @return Long number with identity of Star
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter for Identity
     *
     * @param id Long number with identity of Star
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter for first name
     *
     * @return String with first name of star
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter for first name
     *
     * @param firstName String with first name of star
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter for middle name
     *
     * @return String with middle name of star or null, if doesn't specified
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Setter for middle name
     *
     * @param middleName String with middle name of star
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Getter for last name
     *
     * @return String with last name of star
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter for last name of star
     *
     * @param lastName String with last name of star
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Prints star data
     *
     * @return String with star data
     */
    @Override
    public String toString() {
        return "Star{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Star star = (Star) o;

        return firstName.equals(star.firstName) && (middleName != null ? middleName.equals(star.middleName) : star.middleName == null && lastName.equals(star.lastName));

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + lastName.hashCode();
        return result;
    }
}
