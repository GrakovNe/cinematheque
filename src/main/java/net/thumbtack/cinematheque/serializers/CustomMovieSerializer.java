package net.thumbtack.cinematheque.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.thumbtack.cinematheque.entity.Movie;
import net.thumbtack.cinematheque.entity.Star;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * Custom JSON serializer for movie entity
 */
public class CustomMovieSerializer extends JsonSerializer< Movie > {
    @Override
    public void serialize(Movie movie, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", movie.getId());
        jsonGenerator.writeStringField("title", movie.getTitle());
        jsonGenerator.writeNumberField("year", movie.getYear());

        if (null != movie.getRank()) {
            jsonGenerator.writeNumberField("rank", movie.getRank().getRank());
        }

        if (null != movie.getImage()) {
            jsonGenerator.writeStringField("imageUrl", "/" + movie.getId() + "/image");
        }

        jsonGenerator.writeFieldName("stars");
        jsonGenerator.writeStartArray();

        for (Star star : movie.getStars()) {
            jsonGenerator.writeStartObject();

            jsonGenerator.writeNumberField("id", star.getId());

            StringBuilder builder = new StringBuilder();
            builder.append(star.getFirstName());

            if (!StringUtils.isEmpty(star.getMiddleName())) {
                builder.append(" ").
                        append(star.getMiddleName());
            }

            builder.append(" ").
                    append(star.getLastName());

            jsonGenerator.writeStringField("name", builder.toString());
            jsonGenerator.writeEndObject();
        }

        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
